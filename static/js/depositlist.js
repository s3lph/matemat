Number.prototype.pad = function(size) {
  var s = String(this);
  while (s.length < (size || 2)) {s = "0" + s;}
  return s;
}

const Mode = {
	Deposit: 0,
	Buy: 1,
	Transfer: 2,
}

let mode = Mode.Deposit;
let product_id = null;
let target_user = null;
let target_user_li = null;
let deposit = '0';
let button = document.createElement('div');
let button_transfer = document.createElement('div');
let input = document.getElementById('deposit-wrapper');
let amount = document.getElementById('deposit-amount');
let title = document.getElementById('deposit-title');
let userlist = document.getElementById('transfer-userlist');
let userlist_list = document.getElementById('transfer-userlist-list');
let ok_button = document.getElementById('numpad-ok');
button.classList.add('thumblist-item');
button.classList.add('fakelink');
button.innerText = 'Deposit';
button.onclick = (ev) => {
	mode = Mode.Deposit;
    product_id = null;
	target_user = null;
    deposit = '0';
    title.innerText = 'Deposit';
    amount.innerText = (Math.floor(parseInt(deposit) / 100)) + '.' + (parseInt(deposit) % 100).pad();
    input.classList.add('show');
	userlist.classList.remove('show');
	ok_button.classList.remove('disabled');
};
button_transfer.classList.add('thumblist-item');
button_transfer.classList.add('fakelink');
button_transfer.innerText = 'Transfer';
button_transfer.onclick = (ev) => {
	mode = Mode.Transfer;
    product_id = null;
	target_user = null;
    deposit = '0';
    title.innerText = 'Transfer';
    amount.innerText = (Math.floor(parseInt(deposit) / 100)) + '.' + (parseInt(deposit) % 100).pad();
    input.classList.add('show');
	userlist.classList.add('show');
	ok_button.classList.add('disabled');
};
setup_custom_price = (pid, pname) => {
	mode = Mode.Buy;
    product_id = pid;
	target_user = null;
    title.innerText = pname;
    deposit = '0';
    amount.innerText = (Math.floor(parseInt(deposit) / 100)) + '.' + (parseInt(deposit) % 100).pad();
    input.classList.add('show');
	userlist.classList.remove('show');
	ok_button.classList.remove('disabled');
};
set_transfer_user = (li, uid) => {
	if (target_user_li != null) {
		target_user_li.classList.remove('active');
	}
	target_user = uid;
	target_user_li = li;
	ok_button.classList.remove('disabled');
	target_user_li.classList.add('active');

}
scrollUserlist = (delta) => {
	userlist_list.scrollBy(0, delta);
}
deposit_key = (k) => {
	if (k == 'ok') {
		switch (mode) {
		case Mode.Deposit:
			window.location.href = '/deposit?n=' + parseInt(deposit);
			break;
		case Mode.Buy:
			window.location.href = '/buy?pid=' + product_id + '&price=' + parseInt(deposit);
			break;
		case Mode.Transfer:
			if (target_user == null) {
				return;
			}
			window.location.href = '/transfer?target=' + target_user + '&n=' + parseInt(deposit);
			break;
		}
		mode = Mode.Deposit;
		deposit = '0';
		product_id = null;
		target_user = null;
		if (target_user_li != null) {
			target_user_li.classList.remove('active');
		}
		input.classList.remove('show');
		userlist.classList.remove('show');
    } else if (k == 'del') {
		if (deposit == '0') {
			product_id = null;
			target_user = null;
			if (target_user_li != null) {
				target_user_li.classList.remove('active');
			}
			userlist.classList.remove('show');
			input.classList.remove('show');
		}
		deposit = deposit.substr(0, deposit.length - 1);
		if (deposit.length == 0) {
			deposit = '0';
		}
		amount.innerText = (Math.floor(parseInt(deposit) / 100)) + '.' + (parseInt(deposit) % 100).pad();
	} else {
		if (deposit == '0') {
			deposit = k;
		} else {
			deposit += k;
		}
		amount.innerText = (Math.floor(parseInt(deposit) / 100)) + '.' + (parseInt(deposit) % 100).pad();
    }
};

let list = document.getElementById('depositlist');
list.innerHTML = '';
list.appendChild(button);
list.appendChild(button_transfer);
