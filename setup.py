from setuptools import setup, find_packages

from matemat import __version__

setup(
    name='matemat',
    version=__version__,
    url='https://gitlab.com/s3lph/matemat',
    license='MIT',
    author='s3lph',
    author_email='',
    description='Soda machine stock-keeping webservice',
    long_description='''
A web service for automated stock-keeping of a soda machine written in Python.
It provides a touch-input-friendly user interface (as most input happens through the
soda machine's touch screen).
''',
    
    packages=find_packages(exclude=['*.test']),
    python_requires='>=3.6',
    install_requires=[
        'bottle',
        'file-magic',
        'jinja2',
        'Pillow',
        'netaddr'
    ],
    test_loader='unittest:TestLoader',
    entry_points={
        'console_scripts': [
            'matemat = matemat.__main__:main'
        ]
    }
)
