# Matemat

[![pipeline status](https://gitlab.com/s3lph/matemat/badges/master/pipeline.svg)][master]
[![coverage report](https://gitlab.com/s3lph/matemat/badges/master/coverage.svg)][master]

A web service for automated stock-keeping of a soda machine written in Python.
It provides a touch-input-friendly user interface (as most input happens through the
soda machine's touch screen).

This project intends to provide a well-tested and maintainable alternative to
[ckruse/matemat][oldapp] (last commit 2013-07-09).

## Further Documentation

[Wiki][wiki]

## Dependencies

- Python 3 (>=3.6)
- Python dependencies:
  - file-magic
  - jinja2
  - Pillow
  - netaddr
  
## Usage

```sh
python -m matemat
```

## Contributors

- s3lph
- SPiNNiX
- jonny

## License

[MIT License][mit-license]


[oldapp]: https://github.com/ckruse/matemat
[mit-license]: https://gitlab.com/s3lph/matemat/blob/master/LICENSE
[master]: https://gitlab.com/s3lph/matemat/commits/master
[wiki]: https://gitlab.com/s3lph/matemat/wikis/home