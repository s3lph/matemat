
from typing import Optional


class DatabaseConsistencyError(BaseException):

    def __init__(self, msg: Optional[str] = None) -> None:
        self._msg: Optional[str] = msg

    def __str__(self) -> str:
        return f'DatabaseConsistencyError: {self._msg}'

    @property
    def msg(self) -> Optional[str]:
        return self._msg
