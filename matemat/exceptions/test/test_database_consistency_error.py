
import unittest

from matemat.exceptions import DatabaseConsistencyError


class TestDatabaseConsistencyError(unittest.TestCase):

    def test_msg(self):
        e = DatabaseConsistencyError('testmsg')
        self.assertEqual('testmsg', e.msg)

    def test_str(self):
        e = DatabaseConsistencyError('testmsg')
        self.assertEqual('DatabaseConsistencyError: testmsg', str(e))
