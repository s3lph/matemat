
import unittest

from matemat.exceptions import AuthenticationError


class TestAuthenticationError(unittest.TestCase):

    def test_msg(self):
        e = AuthenticationError('testmsg')
        self.assertEqual('testmsg', e.msg)

    def test_str(self):
        e = AuthenticationError('testmsg')
        self.assertEqual('AuthenticationError: testmsg', str(e))
