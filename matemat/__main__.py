import logging
from typing import Any, Dict, Iterable, Union

import shlex
import sys
import os.path
import bottle

from matemat.db import MatematDatabase
from matemat.webserver import cron
from matemat.webserver.logger import Logger
from matemat.webserver.config import get_config, get_app_config, parse_config_file
from matemat.webserver.template import init as template_init

# Those imports are actually needed, as they implicitly register pagelets.
# noinspection PyUnresolvedReferences
from matemat.webserver.pagelets import *


def _init(config: Dict[str, Any]):
    logger = Logger.instance()
    # Set default values for missing config items
    if 'InstanceName' not in config['pagelet_variables']:
        config['pagelet_variables']['InstanceName'] = 'Matemat'
        logger.warning('Property \'InstanceName\' not set, using \'Matemat\'')
    if 'UploadDir' not in config['pagelet_variables']:
        config['pagelet_variables']['UploadDir'] = './static/upload/'
        logger.warning('Property \'UploadDir\' not set, using \'./static/upload/\'')
    if 'DatabaseFile' not in config['pagelet_variables']:
        config['pagelet_variables']['DatabaseFile'] = './matemat.db'
        logger.warning('Property \'DatabaseFile\' not set, using \'./matemat.db\'')
    if 'SmtpSendReceipts' not in config['pagelet_variables']:
        config['pagelet_variables']['SmtpSendReceipts'] = '0'
        logger.warning('Property \'SmtpSendReceipts\' not set, using \'0\'')
    if config['pagelet_variables']['SmtpSendReceipts'] == '1':
        if 'SmtpFrom' not in config['pagelet_variables']:
            logger.fatal('\'SmtpSendReceipts\' set to \'1\', but \'SmtpFrom\' missing.')
            raise KeyError()
        if 'SmtpSubj' not in config['pagelet_variables']:
            logger.fatal('\'SmtpSendReceipts\' set to \'1\', but \'SmtpSubj\' missing.')
            raise KeyError()
        if 'SmtpHost' not in config['pagelet_variables']:
            logger.fatal('\'SmtpSendReceipts\' set to \'1\', but \'SmtpHost\' missing.')
            raise KeyError()
        if 'SmtpPort' not in config['pagelet_variables']:
            logger.fatal('\'SmtpSendReceipts\' set to \'1\', but \'SmtpPort\' missing.')
            raise KeyError()
        if 'SmtpUser' not in config['pagelet_variables']:
            logger.fatal('\'SmtpSendReceipts\' set to \'1\', but \'SmtpUser\' missing.')
            raise KeyError()
        if 'SmtpPass' not in config['pagelet_variables']:
            logger.fatal('\'SmtpSendReceipts\' set to \'1\', but \'SmtpPass\' missing.')
            raise KeyError()
        if 'SmtpEnforceTLS' not in config['pagelet_variables']:
            config['SmtpEnforceTLS'] = '1'
            logger.warning('Property \'SmtpEnforceTLS\' not set, using \'1\'')
    with MatematDatabase(config['pagelet_variables']['DatabaseFile']):
        # Connect to the database to create it and perform any schema migrations
        pass
    # Initialize Jinaj2 template system
    template_init(config)


@bottle.route('/static/<filename:path>')
def serve_static_files(filename: str):
    config = get_config()
    roots = []
    for themeroot in shlex.split(config['themeroot']):
        roots.append(os.path.abspath(os.path.join(themeroot, config['theme'], 'static')))
    roots.append(os.path.abspath(config['staticroot']))
    resp = None
    for root in roots:
        resp = bottle.static_file(filename, root=root)
        if resp.status_code == 200:
            break
    return resp


def main():
    # Use config file name from command line, if present, and parse it
    configfile: Union[str, Iterable[str]] = '/etc/matemat.conf'
    if len(sys.argv) > 1:
        configfile = sys.argv[1:]
    parse_config_file(configfile)

    config = get_config()

    _init(config)

    host: str = config['listen']
    port: int = int(str(config['port']))

    # noinspection PyUnresolvedReferences
    from matemat.webserver.pagelets.receipt_smtp_cron import receipt_smtp_cron
    # Start the web server
    bottle.run(host=host, port=port)
    # Stop cron
    cron.shutdown()


if __name__ == '__main__':
    main()
