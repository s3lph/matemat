
import unittest

import crypt
from datetime import datetime, timedelta

from matemat.db import MatematDatabase
from matemat.db.primitives import User, Product, ReceiptPreference, Receipt,\
    Transaction, Modification, Deposit, Consumption
from matemat.exceptions import AuthenticationError, DatabaseConsistencyError


class DatabaseTest(unittest.TestCase):

    def setUp(self) -> None:
        # Create an in-memory database for testing
        self.db = MatematDatabase(':memory:')

    def test_create_user(self) -> None:
        with self.db as db:
            with db.transaction(exclusive=False) as c:
                db.create_user('testuser', 'supersecurepassword', 'testuser@example.com')
                c.execute("SELECT * FROM users")
                row = c.fetchone()
                self.assertEqual('testuser', row[1])
                self.assertEqual('testuser@example.com', row[2])
                self.assertEqual(0, row[5])
                self.assertEqual(1, row[6])
                self.assertEqual(ReceiptPreference.NONE.value, row[7])
                with self.assertRaises(ValueError):
                    db.create_user('testuser', 'supersecurepassword2', 'testuser2@example.com')

    def test_get_user(self) -> None:
        with self.db as db:
            with db.transaction() as c:
                created = db.create_user('testuser', 'supersecurepassword', 'testuser@example.com',
                                         admin=True, member=False)
                user = db.get_user(created.id)
                self.assertEqual('testuser', user.name)
                self.assertEqual('testuser@example.com', user.email)
                self.assertEqual(False, user.is_member)
                self.assertEqual(True, user.is_admin)
                self.assertEqual(ReceiptPreference.NONE, user.receipt_pref)
                with self.assertRaises(ValueError):
                    db.get_user(-1)
                # Write an invalid receipt preference to the database
                c.execute('UPDATE users SET receipt_pref = 42 WHERE user_id = ?',
                          [user.id])
                with self.assertRaises(DatabaseConsistencyError):
                    db.get_user(user.id)

    def test_list_users(self) -> None:
        with self.db as db:
            users = db.list_users()
            self.assertEqual(0, len(users))
            users = db.list_users(with_touchkey=True)
            self.assertEqual(0, len(users))
            testuser: User = db.create_user('testuser', 'supersecurepassword', 'testuser@example.com', True, True)
            db.change_touchkey(testuser, '', 'touchkey', verify_password=False)
            db.create_user('anothertestuser', 'otherpassword', 'anothertestuser@example.com', False, True)
            u = db.create_user('yatu', 'igotapasswordtoo', 'yatu@example.com', False, False)
            db.change_user(u, agent=None, receipt_pref=ReceiptPreference.WEEKLY)
            users = db.list_users()
            users_with_touchkey = db.list_users(with_touchkey=True)
        self.assertEqual(3, len(users))
        usercheck = {}
        for user in users:
            if user.name == 'testuser':
                self.assertEqual('testuser@example.com', user.email)
                self.assertTrue(user.is_member)
                self.assertTrue(user.is_admin)
            elif user.name == 'anothertestuser':
                self.assertEqual('anothertestuser@example.com', user.email)
                self.assertTrue(user.is_member)
                self.assertFalse(user.is_admin)
            elif user.name == 'yatu':
                self.assertEqual('yatu@example.com', user.email)
                self.assertFalse(user.is_member)
                self.assertFalse(user.is_admin)
                self.assertEqual(ReceiptPreference.WEEKLY, user.receipt_pref)
            usercheck[user.id] = 1
        self.assertEqual(3, len(usercheck))
        self.assertEqual(1, len(users_with_touchkey))
        self.assertEqual('testuser', users_with_touchkey[0].name)

    def test_has_admin_users(self):
        with self.db as db:
            self.assertFalse(db.has_admin_users())
            testuser: User = db.create_user('testuser', 'supersecurepassword', 'testuser@example.com', True, True)
            self.assertTrue(db.has_admin_users())
            db.change_user(testuser, agent=testuser, is_admin=False)
            self.assertFalse(db.has_admin_users())

    def test_login(self) -> None:
        with self.db as db:
            with db.transaction() as c:
                u = db.create_user('testuser', 'supersecurepassword', 'testuser@example.com')
                # Attempt touchkey login without a set touchkey
                try:
                    db.login('testuser', touchkey='0123')
                    self.fail()
                except AuthenticationError as e:
                    self.assertEqual('Touchkey not set', e.msg)
                # Add a touchkey without using the provided function
                c.execute('''UPDATE users SET touchkey = :tkhash, receipt_pref = 2 WHERE user_id = :user_id''', {
                    'tkhash': crypt.crypt('0123', crypt.mksalt()),
                    'user_id': u.id
                })
            user = db.login('testuser', 'supersecurepassword')
            self.assertEqual(u.id, user.id)
            user = db.login('testuser', touchkey='0123')
            self.assertEqual(u.id, user.id)
            self.assertEqual(ReceiptPreference.MONTHLY, user.receipt_pref)
            with self.assertRaises(AuthenticationError):
                # Inexistent user should fail
                db.login('nooone', 'supersecurepassword')
            with self.assertRaises(AuthenticationError):
                # Wrong password should fail
                db.login('testuser', 'anothersecurepassword')
            with self.assertRaises(AuthenticationError):
                # Wrong touchkey should fail
                db.login('testuser', touchkey='0124')
            with self.assertRaises(ValueError):
                # No password or touchkey should fail
                db.login('testuser')
            with self.assertRaises(ValueError):
                # Both password and touchkey should fail
                db.login('testuser', password='supersecurepassword', touchkey='0123')

    def test_change_password(self) -> None:
        with self.db as db:
            user = db.create_user('testuser', 'supersecurepassword', 'testuser@example.com')
            db.login('testuser', 'supersecurepassword')
            # Normal password change should succeed
            db.change_password(user, 'supersecurepassword', 'mynewpassword')
            with self.assertRaises(AuthenticationError):
                db.login('testuser', 'supersecurepassword')
            db.login('testuser', 'mynewpassword')
            with self.assertRaises(AuthenticationError):
                # Should fail due to wrong old password
                db.change_password(user, 'iforgotmypassword', 'mynewpassword')
            db.login('testuser', 'mynewpassword')
            # This should pass even though the old password is not known (admin password reset)
            db.change_password(user, '42', 'adminpasswordreset', verify_password=False)
            with self.assertRaises(AuthenticationError):
                db.login('testuser', 'mynewpassword')
            db.login('testuser', 'adminpasswordreset')
            user.id = -1
            with self.assertRaises(AuthenticationError):
                # Password change for an inexistent user should fail
                db.change_password(user, 'adminpasswordreset', 'passwordwithoutuser')

    def test_change_touchkey(self) -> None:
        with self.db as db:
            user = db.create_user('testuser', 'supersecurepassword', 'testuser@example.com')
            db.change_touchkey(user, 'supersecurepassword', '0123')
            db.login('testuser', touchkey='0123')
            # Normal touchkey change should succeed
            db.change_touchkey(user, 'supersecurepassword', touchkey='4567')
            with self.assertRaises(AuthenticationError):
                db.login('testuser', touchkey='0123')
            db.login('testuser', touchkey='4567')
            with self.assertRaises(AuthenticationError):
                # Should fail due to wrong old password
                db.change_touchkey(user, 'iforgotmypassword', '89ab')
            db.login('testuser', touchkey='4567')
            # This should pass even though the old password is not known (admin password reset)
            db.change_touchkey(user, '42', '89ab', verify_password=False)
            with self.assertRaises(AuthenticationError):
                db.login('testuser', touchkey='4567')
            db.login('testuser', touchkey='89ab')
            user.id = -1
            with self.assertRaises(AuthenticationError):
                # Touchkey change for an inexistent user should fail
                db.change_touchkey(user, '89ab', '048c')

    def test_change_user(self) -> None:
        with self.db as db:
            agent = db.create_user('admin', 'supersecurepassword', 'admin@example.com', True, True)
            user = db.create_user('testuser', 'supersecurepassword', 'testuser@example.com', True, True)
            db.change_user(user, agent, email='newaddress@example.com', is_admin=False, is_member=False, balance=4200,
                           balance_reason='This is a reason!', receipt_pref=ReceiptPreference.MONTHLY)
            # Changes must be reflected in the passed user object
            self.assertEqual('newaddress@example.com', user.email)
            self.assertFalse(user.is_admin)
            self.assertFalse(user.is_member)
            self.assertEqual(4200, user.balance)
            self.assertEqual(ReceiptPreference.MONTHLY, user.receipt_pref)
            # Changes must be reflected in the database
            checkuser = db.get_user(user.id)
            self.assertEqual('newaddress@example.com', user.email)
            self.assertFalse(checkuser.is_admin)
            self.assertFalse(checkuser.is_member)
            self.assertEqual(4200, checkuser.balance)
            self.assertEqual(ReceiptPreference.MONTHLY, checkuser.receipt_pref)
            with db.transaction(exclusive=False) as c:
                c.execute('SELECT reason FROM modifications LIMIT 1')
                self.assertEqual('This is a reason!', c.fetchone()[0])
            # Balance change without an agent must fail
            with self.assertRaises(ValueError):
                db.change_user(user, None, balance=0)
            user.id = -1
            with self.assertRaises(DatabaseConsistencyError):
                db.change_user(user, agent, is_member='True')

    def test_delete_user(self) -> None:
        with self.db as db:
            user = db.create_user('testuser', 'supersecurepassword', 'testuser@example.com', True, True)
            db.login('testuser', 'supersecurepassword')
            db.delete_user(user)
            try:
                # Should fail, as the user does not exist anymore
                db.login('testuser', 'supersecurepassword')
            except AuthenticationError as e:
                self.assertEqual('User does not exist', e.msg)
            with self.assertRaises(DatabaseConsistencyError):
                # Should fail, as the user does not exist anymore
                db.delete_user(user)

    def test_create_product(self) -> None:
        with self.db as db:
            with db.transaction() as c:
                db.create_product('Club Mate', 200, 200, True, True)
                c.execute("SELECT * FROM products")
                row = c.fetchone()
                self.assertEqual('Club Mate', row[1])
                self.assertEqual(0, row[2])
                self.assertEqual(1, row[3])
                self.assertEqual(200, row[4])
                self.assertEqual(200, row[5])
                self.assertEqual(1, row[6])
            with self.assertRaises(ValueError):
                db.create_product('Club Mate', 250, 250, False, False)

    def test_get_product(self) -> None:
        with self.db as db:
            with db.transaction(exclusive=False):
                created = db.create_product('Club Mate', 150, 250, False, False)
                product = db.get_product(created.id)
                self.assertEqual('Club Mate', product.name)
                self.assertEqual(150, product.price_member)
                self.assertEqual(250, product.price_non_member)
                self.assertEqual(False, product.stockable)
                with self.assertRaises(ValueError):
                    db.get_product(-1)

    def test_list_products(self) -> None:
        with self.db as db:
            # Test empty list
            products = db.list_products()
            self.assertEqual(0, len(products))
            db.create_product('Club Mate', 200, 200, False, True)
            db.create_product('Flora Power Mate', 200, 200, False, False)
            db.create_product('Fritz Mate', 200, 250, False, True)
            products = db.list_products()
            self.assertEqual(3, len(products))
        productcheck = {}
        for product in products:
            if product.name == 'Club Mate':
                self.assertEqual(200, product.price_member)
                self.assertEqual(200, product.price_non_member)
                self.assertTrue(product.stockable)
            elif product.name == 'Flora Power Mate':
                self.assertEqual(200, product.price_member)
                self.assertEqual(200, product.price_non_member)
                self.assertFalse(product.stockable)
            elif product.name == 'Fritz Mate':
                self.assertEqual(200, product.price_member)
                self.assertEqual(250, product.price_non_member)
                self.assertTrue(product.stockable)
            productcheck[product.id] = 1
        self.assertEqual(3, len(productcheck))

    def test_change_product(self) -> None:
        with self.db as db:
            product = db.create_product('Club Mate', 200, 200, False, True)
            db.change_product(product, name='Flora Power Mate', price_member=150, price_non_member=250,
                              custom_price=True, stock=None, stockable=False)
            # Changes must be reflected in the passed object
            self.assertEqual('Flora Power Mate', product.name)
            self.assertEqual(150, product.price_member)
            self.assertEqual(250, product.price_non_member)
            self.assertEqual(True, product.custom_price)
            self.assertEqual(None, product.stock)
            self.assertEqual(False, product.stockable)
            # Changes must be reflected in the database
            checkproduct = db.get_product(product.id)
            self.assertEqual('Flora Power Mate', checkproduct.name)
            self.assertEqual(150, checkproduct.price_member)
            self.assertEqual(250, checkproduct.price_non_member)
            self.assertEqual(True, checkproduct.custom_price)
            self.assertEqual(None, checkproduct.stock)
            self.assertEqual(False, checkproduct.stockable)
            product.id = -1
            with self.assertRaises(DatabaseConsistencyError):
                db.change_product(product)
            product2 = db.create_product('Club Mate', 200, 200, False, True)
            product2.name = 'Flora Power Mate'
            with self.assertRaises(DatabaseConsistencyError):
                # Should fail, as a product with the same name already exists.
                db.change_product(product2)

    def test_delete_product(self) -> None:
        with self.db as db:
            product = db.create_product('Club Mate', 200, 200, False, True)
            product2 = db.create_product('Flora Power Mate', 200, 200, False, False)

            self.assertEqual(2, len(db.list_products()))
            db.delete_product(product)

            # Only the other product should remain in the DB
            products = db.list_products()
            self.assertEqual(1, len(products))
            self.assertEqual(product2, products[0])

            with self.assertRaises(DatabaseConsistencyError):
                # Should fail, as the product does not exist anymore
                db.delete_product(product)

    def test_deposit(self) -> None:
        with self.db as db:
            with db.transaction() as c:
                user = db.create_user('testuser', 'supersecurepassword', 'testuser@example.com', True, True)
                user2 = db.create_user('testuser2', 'supersecurepassword', 'testuser@example.com', True, True)
                c.execute('''SELECT balance FROM users WHERE user_id = ?''', [user.id])
                self.assertEqual(0, c.fetchone()[0])
                c.execute('''SELECT balance FROM users WHERE user_id = ?''', [user2.id])
                self.assertEqual(0, c.fetchone()[0])
                db.deposit(user, 1337)
                c.execute('''SELECT balance FROM users WHERE user_id = ?''', [user.id])
                self.assertEqual(1337, c.fetchone()[0])
                c.execute('''SELECT balance FROM users WHERE user_id = ?''', [user2.id])
                self.assertEqual(0, c.fetchone()[0])
                with self.assertRaises(ValueError):
                    # Should fail, negative amount
                    db.deposit(user, -42)
                user.id = -1
                with self.assertRaises(DatabaseConsistencyError):
                    # Should fail, user id -1 does not exist
                    db.deposit(user, 42)

    def test_transfer(self) -> None:
        with self.db as db:
            with db.transaction() as c:
                user = db.create_user('testuser', 'supersecurepassword', 'testuser@example.com', True, True)
                user2 = db.create_user('testuser2', 'supersecurepassword', 'testuser@example.com', True, True)
                user3 = db.create_user('testuser3', 'supersecurepassword', 'testuser@example.com', True, True)
                c.execute('''SELECT balance FROM users WHERE user_id = ?''', [user.id])
                self.assertEqual(0, c.fetchone()[0])
                c.execute('''SELECT balance FROM users WHERE user_id = ?''', [user2.id])
                self.assertEqual(0, c.fetchone()[0])
                c.execute('''SELECT balance FROM users WHERE user_id = ?''', [user3.id])
                self.assertEqual(0, c.fetchone()[0])
                db.transfer(user, user2, 1337)
                c.execute('''SELECT balance FROM users WHERE user_id = ?''', [user.id])
                self.assertEqual(-1337, c.fetchone()[0])
                c.execute('''SELECT balance FROM users WHERE user_id = ?''', [user2.id])
                self.assertEqual(1337, c.fetchone()[0])
                c.execute('''SELECT balance FROM users WHERE user_id = ?''', [user3.id])
                self.assertEqual(0, c.fetchone()[0])
                with self.assertRaises(ValueError):
                    # Should fail, negative amount
                    db.transfer(user, user2, -42)
                user.id = -1
                with self.assertRaises(DatabaseConsistencyError):
                    # Should fail, user id -1 does not exist
                    db.transfer(user, user2, 42)
                with self.assertRaises(DatabaseConsistencyError):
                    # Should fail, user id -1 does not exist
                    db.transfer(user2, user, 42)

    def test_consumption(self) -> None:
        with self.db as db:
            # Set up test case
            user1 = db.create_user('user1', 'supersecurepassword', 'testuser@example.com', member=True)
            user2 = db.create_user('user2', 'supersecurepassword', 'testuser@example.com', member=False)
            user3 = db.create_user('user3', 'supersecurepassword', 'testuser@example.com', member=False)
            db.deposit(user1, 1337)
            db.deposit(user2, 4242)
            db.deposit(user3, 1234)
            clubmate = db.create_product('Club Mate', 200, 200, False, True)
            florapowermate = db.create_product('Flora Power Mate', 150, 250, False, True)
            fritzmate = db.create_product('Fritz Mate', 200, 200, False, True)

            # user1 is somewhat addicted to caffeine
            for _ in range(3):
                db.increment_consumption(user1, clubmate)
                db.increment_consumption(user1, florapowermate)

            # user2 is reeeally addicted
            for _ in range(7):
                db.increment_consumption(user2, clubmate)
                db.increment_consumption(user2, florapowermate)

            with db.transaction(exclusive=False) as c:
                c.execute('''SELECT balance FROM users WHERE user_id = ?''', [user1.id])
                self.assertEqual(1337 - 200 * 3 - 150 * 3, c.fetchone()[0])
                c.execute('''SELECT balance FROM users WHERE user_id = ?''', [user2.id])
                self.assertEqual(4242 - 200 * 7 - 250 * 7, c.fetchone()[0])
                c.execute('''SELECT balance FROM users WHERE user_id = ?''', [user3.id])
                self.assertEqual(1234, c.fetchone()[0])

            user1.id = -1
            with self.assertRaises(DatabaseConsistencyError):
                db.increment_consumption(user1, florapowermate)

    def test_check_receipt_due(self):
        with self.db as db:
            # Receipt preference set to 0
            user0 = db.create_user('user0', 'supersecurepassword', 'user0@example.com', True, True)
            # No email, no receipts
            user1 = db.create_user('user1', 'supersecurepassword', None, True, True)
            db.change_user(user1, agent=None, receipt_pref=ReceiptPreference.MONTHLY)
            # Should receive a receipt, has never received a receipt before
            user2 = db.create_user('user2', 'supersecurepassword', 'user2@example.com', True, True)
            db.change_user(user2, agent=None, receipt_pref=ReceiptPreference.MONTHLY)
            # Should receive a receipt, has received receipts before
            user3 = db.create_user('user3', 'supersecurepassword', 'user3@example.com', True, True)
            db.change_user(user3, agent=None, receipt_pref=ReceiptPreference.MONTHLY)
            # Shouldn't receive a receipt, a month hasn't passed since the last receipt
            user4 = db.create_user('user4', 'supersecurepassword', 'user4@example.com', True, True)
            db.change_user(user4, agent=None, receipt_pref=ReceiptPreference.MONTHLY)
            # Should receive a receipt, has been more than a year since the last receipt
            user5 = db.create_user('user5', 'supersecurepassword', 'user5@example.com', True, True)
            db.change_user(user5, agent=None, receipt_pref=ReceiptPreference.YEARLY)
            # Shouldn't receive a receipt, a year hasn't passed since the last receipt
            user6 = db.create_user('user6', 'supersecurepassword', 'user6@example.com', True, True)
            db.change_user(user6, agent=None, receipt_pref=ReceiptPreference.YEARLY)
            # Invalid receipt preference, should raise a ValueError
            user7 = db.create_user('user7', 'supersecurepassword', 'user7@example.com', True, True)
            user7.receipt_pref = 42

            twoyears: int = int((datetime.utcnow() - timedelta(days=730)).timestamp())
            halfyear: int = int((datetime.utcnow() - timedelta(days=183)).timestamp())
            twomonths: int = int((datetime.utcnow() - timedelta(days=61)).timestamp())
            halfmonth: int = int((datetime.utcnow() - timedelta(days=15)).timestamp())

            with db.transaction() as c:
                # Fix creation date for user2
                c.execute('''
                UPDATE users SET created = :twomonths WHERE user_id = :user2
                ''', {
                    'twomonths': twomonths,
                    'user2': user2.id
                })
                # Create transactions
                c.execute('''
                INSERT INTO transactions (ta_id, user_id, value, old_balance, date) VALUES
                  (1, :user0, 4200, 0, :twomonths),
                  (2, :user0, 100, 4200, :halfmonth),
                  (3, :user1, 4200, 0, :twomonths),
                  (4, :user1, 100, 4200, :halfmonth),
                  (5, :user2, 4200, 0, :twomonths),
                  (6, :user2, 100, 4200, :halfmonth),
                  (7, :user3, 4200, 0, :twomonths),
                  (8, :user3, 100, 4200, :halfmonth),
                  (9, :user4, 4200, 0, :twomonths),
                  (10, :user4, 100, 4200, :halfmonth),
                  (11, :user5, 4200, 0, :twoyears),
                  (12, :user5, 100, 4200, :halfyear),
                  (13, :user6, 4200, 0, :twoyears),
                  (14, :user6, 100, 4200, :halfyear)
                ''', {
                    'twoyears': twoyears,
                    'halfyear': halfyear,
                    'twomonths': twomonths,
                    'halfmonth': halfmonth,
                    'user0': user0.id,
                    'user1': user1.id,
                    'user2': user2.id,
                    'user3': user3.id,
                    'user4': user4.id,
                    'user5': user5.id,
                    'user6': user6.id
                })
                # Create receipts
                c.execute('''
                INSERT INTO receipts (user_id, first_ta_id, last_ta_id, date) VALUES
                  (:user3, 7, 7, :twomonths),
                  (:user4, 9, 9, :halfmonth),
                  (:user5, 11, 11, :twoyears),
                  (:user6, 13, 13, :halfyear)
                ''', {
                    'twoyears': twoyears,
                    'halfyear': halfyear,
                    'twomonths': twomonths,
                    'halfmonth': halfmonth,
                    'user3': user3.id,
                    'user4': user4.id,
                    'user5': user5.id,
                    'user6': user6.id
                })

            self.assertFalse(db.check_receipt_due(user0))
            self.assertFalse(self.db.check_receipt_due(user1))
            self.assertTrue(self.db.check_receipt_due(user2))
            self.assertTrue(self.db.check_receipt_due(user3))
            self.assertFalse(self.db.check_receipt_due(user4))
            self.assertTrue(self.db.check_receipt_due(user5))
            self.assertFalse(self.db.check_receipt_due(user6))
            with self.assertRaises(TypeError):
                self.db.check_receipt_due(user7)

    def test_create_receipt(self):
        with self.db as db:

            admin: User = db.create_user('admin', 'supersecurepassword', 'admin@example.com', True, True)
            user: User = db.create_user('user', 'supersecurepassword', 'user@example.com', True, True)
            product: Product = db.create_product('Flora Power Mate', 200, 200, False, True)

            # Create some transactions
            db.change_user(user, agent=admin,
                           receipt_pref=ReceiptPreference.MONTHLY,
                           balance=4200, balance_reason='Here\'s a gift!')
            db.increment_consumption(user, product)
            db.deposit(user, 1337)
            receipt1: Receipt = db.create_receipt(user, write=True)
            # Attempt to create a receipt with zero transactions. Will carry NULL transaction IDs
            receipt2: Receipt = db.create_receipt(user, write=True)
            self.assertNotEqual(-1, receipt2.id)
            self.assertEqual(0, len(receipt2.transactions))

            with db.transaction() as c:
                c.execute('SELECT COUNT(receipt_id) FROM receipts')
                self.assertEqual(2, c.fetchone()[0])

            db.increment_consumption(user, product)
            db.change_user(user, agent=admin, balance=4200)
            with db.transaction() as c:
                # Unknown transactions
                c.execute('''
                INSERT INTO transactions (user_id, value, old_balance)
                SELECT user_id, 500, balance
                FROM users
                WHERE user_id = :id
                ''', [user.id])
            receipt3: Receipt = db.create_receipt(user, write=False)

            with db.transaction() as c:
                c.execute('SELECT COUNT(receipt_id) FROM receipts')
                self.assertEqual(2, c.fetchone()[0])

            self.assertEqual(user, receipt1.user)
            self.assertEqual(3, len(receipt1.transactions))

            self.assertIsInstance(receipt1.transactions[0], Modification)
            t10: Modification = receipt1.transactions[0]
            self.assertEqual(user, receipt1.user)
            self.assertEqual(4200, t10.value)
            self.assertEqual(0, t10.old_balance)
            self.assertEqual(admin.name, t10.agent)
            self.assertEqual('Here\'s a gift!', t10.reason)

            self.assertIsInstance(receipt1.transactions[1], Consumption)
            t11: Consumption = receipt1.transactions[1]
            self.assertEqual(user, receipt1.user)
            self.assertEqual(-200, t11.value)
            self.assertEqual(4200, t11.old_balance)
            self.assertEqual('Flora Power Mate', t11.product)

            self.assertIsInstance(receipt1.transactions[2], Deposit)
            t12: Deposit = receipt1.transactions[2]
            self.assertEqual(user, receipt1.user)
            self.assertEqual(1337, t12.value)
            self.assertEqual(4000, t12.old_balance)

            self.assertEqual(user, receipt3.user)
            self.assertEqual(3, len(receipt3.transactions))

            self.assertIsInstance(receipt3.transactions[0], Consumption)
            t20: Consumption = receipt3.transactions[0]
            self.assertEqual(user, receipt3.user)
            self.assertEqual(-200, t20.value)
            self.assertEqual(5337, t20.old_balance)
            self.assertEqual('Flora Power Mate', t20.product)

            self.assertIsInstance(receipt3.transactions[1], Modification)
            t21: Modification = receipt3.transactions[1]
            self.assertEqual(user, receipt3.user)
            self.assertEqual(-937, t21.value)
            self.assertEqual(5137, t21.old_balance)
            self.assertEqual(admin.name, t21.agent)
            self.assertEqual(None, t21.reason)

            self.assertIs(type(receipt3.transactions[2]), Transaction)
            t22: Transaction = receipt3.transactions[2]
            self.assertEqual(user, receipt3.user)
            self.assertEqual(500, t22.value)
            self.assertEqual(4200, t22.old_balance)

    def test_generate_sales_statistics(self):
        with self.db as db:

            user1: User = db.create_user('user1', 'supersecurepassword', 'user1@example.com', True, False)
            user2: User = db.create_user('user2', 'supersecurepassword', 'user2@example.com', True, False)
            user3: User = db.create_user('user3', 'supersecurepassword', 'user3@example.com', True, False)
            user4: User = db.create_user('user4', 'supersecurepassword', 'user4@example.com', True, False)
            flora: Product = db.create_product('Flora Power Mate', 200, 200, False, True)
            club: Product = db.create_product('Club Mate', 200, 200, False, False)

            # Create some transactions
            db.deposit(user1, 1337)
            db.deposit(user2, 4200)
            db.deposit(user3, 1337)
            db.deposit(user4, 4200)
            for _ in range(10):
                db.increment_consumption(user1, flora)
                db.increment_consumption(user2, flora)
                db.increment_consumption(user3, club)
                db.increment_consumption(user4, club)

            # Generate statistics
            now = datetime.utcnow()
            stats = db.generate_sales_statistics(now - timedelta(days=1), now + timedelta(days=1))

            self.assertEqual(7, len(stats))
            self.assertEqual(8000, stats['total_income'])
            self.assertEqual(40, stats['total_consumption'])
            self.assertEqual(3074, stats['total_balance'])
            self.assertEqual(4400, stats['positive_balance'])
            self.assertEqual(-1326, stats['negative_balance'])
            self.assertEqual(11074, stats['total_deposits'])
            self.assertIn('consumptions', stats)

            consumptions = stats['consumptions']
            self.assertEqual(2, len(consumptions))
            self.assertEqual(-4000, consumptions['Flora Power Mate'][0])
            self.assertEqual(20, consumptions['Flora Power Mate'][1])
            self.assertEqual(-4000, consumptions['Club Mate'][0])
            self.assertEqual(20, consumptions['Club Mate'][1])

# TODO: Test cases for primitive object vs database row mismatch
