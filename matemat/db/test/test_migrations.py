
import unittest

import sqlite3

from matemat.db import DatabaseWrapper
from matemat.db.schemas import SCHEMAS


class TestMigrations(unittest.TestCase):

    def setUp(self):
        # Create an in-memory database for testing
        self.db = DatabaseWrapper(':memory:')

    def _initialize_db(self, schema_version: int):
        self.db._sqlite_db = sqlite3.connect(':memory:')
        cursor: sqlite3.Cursor = self.db._sqlite_db.cursor()
        cursor.execute('BEGIN EXCLUSIVE')
        for cmd in SCHEMAS[schema_version]:
            cursor.execute(cmd)
        cursor.execute('COMMIT')

    def test_downgrade_fail(self):
        # Test that downgrades are forbidden
        self.db.SCHEMA_VERSION = 1
        self.db._sqlite_db = sqlite3.connect(':memory:')
        self.db._sqlite_db.execute('PRAGMA user_version = 2')
        with self.assertRaises(RuntimeError):
            with self.db:
                pass

    def test_upgrade_1_to_2(self):
        # Setup test db with example entries covering - hopefully - all cases
        self._initialize_db(1)
        cursor: sqlite3.Cursor = self.db._sqlite_db.cursor()
        cursor.execute('''
            INSERT INTO users VALUES
              (1, 'testadmin', 'a@b.c', '$2a$10$herebehashes', NULL, 1, 1, 1337, 0),
              (2, 'testuser', NULL, '$2a$10$herebehashes', '$2a$10$herebehashes', 0, 1, 4242, 0),
              (3, 'alien', NULL, '$2a$10$herebehashes', '$2a$10$herebehashes', 0, 0, 1234, 0)
            ''')
        cursor.execute('''
            INSERT INTO products VALUES
              (1, 'Club Mate', 42, 200, 250),
              (2, 'Flora Power Mate (1/4l)', 10, 100, 150)
            ''')
        cursor.execute('''
            INSERT INTO consumption VALUES
              (1, 1, 5), (1, 2, 3), (2, 2, 10), (3, 1, 3), (3, 2, 4)
            ''')
        cursor.execute('PRAGMA user_version = 1')

        # Kick off the migration
        schema_version = self.db.SCHEMA_VERSION
        self.db.SCHEMA_VERSION = 2
        self.db._setup()
        self.db.SCHEMA_VERSION = schema_version

        # Test whether the new tables were created
        cursor.execute('PRAGMA table_info(transactions)')
        self.assertNotEqual(0, len(cursor.fetchall()))
        cursor.execute('PRAGMA table_info(consumptions)')
        self.assertNotEqual(0, len(cursor.fetchall()))
        cursor.execute('PRAGMA table_info(deposits)')
        self.assertNotEqual(0, len(cursor.fetchall()))
        cursor.execute('PRAGMA table_info(modifications)')
        self.assertNotEqual(0, len(cursor.fetchall()))
        # Test whether the old consumption table was dropped
        cursor.execute('PRAGMA table_info(consumption)')
        self.assertEqual(0, len(cursor.fetchall()))

        # Test number of entries in the new tables
        cursor.execute('SELECT COUNT(ta_id) FROM transactions')
        self.assertEqual(25, cursor.fetchone()[0])
        cursor.execute('SELECT COUNT(ta_id) FROM consumptions')
        self.assertEqual(25, cursor.fetchone()[0])
        cursor.execute('SELECT COUNT(ta_id) FROM deposits')
        self.assertEqual(0, cursor.fetchone()[0])
        cursor.execute('SELECT COUNT(ta_id) FROM modifications')
        self.assertEqual(0, cursor.fetchone()[0])

        # The (user_id=2 x product_id=1) combination should never appear
        cursor.execute('''
            SELECT COUNT(t.ta_id)
              FROM transactions AS t
              LEFT JOIN consumptions AS c
                ON t.ta_id = c.ta_id
              WHERE t.user_id = 2 AND c.product_id = 1''')
        self.assertEqual(0, cursor.fetchone()[0])

        # Test that one entry per consumption was created, and their values match the negative price
        cursor.execute('''
            SELECT COUNT(t.ta_id)
              FROM transactions AS t
              LEFT JOIN consumptions AS c
                ON t.ta_id = c.ta_id
              WHERE t.user_id = 1 AND c.product_id = 1 AND t.value = -200''')
        self.assertEqual(5, cursor.fetchone()[0])
        cursor.execute('''
            SELECT COUNT(t.ta_id)
              FROM transactions AS t
              LEFT JOIN consumptions AS c
                ON t.ta_id = c.ta_id
              WHERE t.user_id = 1 AND c.product_id = 2 AND t.value = -100''')
        self.assertEqual(3, cursor.fetchone()[0])
        cursor.execute('''
            SELECT COUNT(t.ta_id)
              FROM transactions AS t
              LEFT JOIN consumptions AS c
                ON t.ta_id = c.ta_id
              WHERE t.user_id = 2 AND c.product_id = 2 AND t.value = -100''')
        self.assertEqual(10, cursor.fetchone()[0])
        cursor.execute('''
            SELECT COUNT(t.ta_id)
              FROM transactions AS t
              LEFT JOIN consumptions AS c
                ON t.ta_id = c.ta_id
              WHERE t.user_id = 3 AND c.product_id = 1 AND t.value = -250''')
        self.assertEqual(3, cursor.fetchone()[0])
        cursor.execute('''
            SELECT COUNT(t.ta_id)
              FROM transactions AS t
              LEFT JOIN consumptions AS c
                ON t.ta_id = c.ta_id
              WHERE t.user_id = 3 AND c.product_id = 2 AND t.value = -150''')
        self.assertEqual(4, cursor.fetchone()[0])

    def test_upgrade_2_to_3(self):
        # Setup test db with example entries covering - hopefully - all cases
        self._initialize_db(2)
        cursor: sqlite3.Cursor = self.db._sqlite_db.cursor()
        cursor.execute('''
            INSERT INTO users VALUES
              (1, 'testadmin', 'a@b.c', '$2a$10$herebehashes', NULL, 1, 1, 1337, 0),
              (2, 'testuser', NULL, '$2a$10$herebehashes', '$2a$10$herebehashes', 0, 1, 4242, 0),
              (3, 'alien', NULL, '$2a$10$herebehashes', '$2a$10$herebehashes', 0, 0, 1234, 0),
              (4, 'neverused', NULL, '$2a$10$herebehashes', '$2a$10$herebehashes', 0, 0, 1234, 1234)
        ''')
        cursor.execute('''
            INSERT INTO products VALUES
              (1, 'Club Mate', 42, 200, 250),
              (2, 'Flora Power Mate', 10, 100, 150)
        ''')
        cursor.execute('''
            INSERT INTO transactions VALUES
              (1, 1, 4200, 0, 1000),  -- deposit
              (2, 2, 1337, 0, 1001),  -- modification
              (3, 3, 1337, 0, 1002),  -- modification with deleted agent
              (4, 2, -200, 1337, 1003),  -- consumption
              (5, 3, -200, 1337, 1004)  -- consumption with deleted product
        ''')
        cursor.execute('''INSERT INTO deposits VALUES (1)''')
        cursor.execute('''
            INSERT INTO modifications VALUES
              (2, 1, 'Account migration'),
              (3, 42, 'You can''t find out who i am... MUAHAHAHA!!!')''')
        cursor.execute('''INSERT INTO consumptions VALUES (4, 2), (5, 42)''')
        cursor.execute('''PRAGMA user_version = 2''')

        # Kick off the migration
        schema_version = self.db.SCHEMA_VERSION
        self.db.SCHEMA_VERSION = 3
        self.db._setup()
        self.db.SCHEMA_VERSION = schema_version

        # Make sure the receipts table was created
        cursor.execute('''SELECT COUNT(receipt_id) FROM receipts''')
        self.assertEqual(0, cursor.fetchone()[0])

        # Make sure users.created was populated with the expected values
        cursor.execute('''SELECT u.created FROM users AS u ORDER BY u.user_id ASC''')
        self.assertEqual([(940,), (941,), (942,), (1174,)], cursor.fetchall())

        # Make sure the modifications table was changed to contain the username, or a fallback
        cursor.execute('''SELECT agent FROM modifications WHERE ta_id = 2''')
        self.assertEqual('testadmin', cursor.fetchone()[0])
        cursor.execute('''SELECT agent FROM modifications WHERE ta_id = 3''')
        self.assertEqual('<unknown>', cursor.fetchone()[0])

        # Make sure the consumptions table was changed to contain the product name, or a fallback
        cursor.execute('''SELECT product FROM consumptions WHERE ta_id = 4''')
        self.assertEqual('Flora Power Mate', cursor.fetchone()[0])
        cursor.execute('''SELECT product FROM consumptions WHERE ta_id = 5''')
        self.assertEqual('<unknown>', cursor.fetchone()[0])

    def test_upgrade_3_to_4(self):
        # Setup test db with example entries to test schema change
        self._initialize_db(3)
        cursor: sqlite3.Cursor = self.db._sqlite_db.cursor()
        cursor.execute('''
            INSERT INTO users VALUES
              (1, 'testadmin', 'a@b.c', '$2a$10$herebehashes', NULL, 1, 1, 1337, 0, 0, 0)
        ''')
        cursor.execute('''
            INSERT INTO products VALUES
              (1, 'Club Mate', 42, 200, 250)
        ''')
        cursor.execute('''
            INSERT INTO transactions VALUES (1, 1, 4200, 0, 1000)
        ''')
        cursor.execute('''
            INSERT INTO receipts VALUES (1, 1, 1, 1, 1337)
        ''')
        cursor.execute('PRAGMA user_version = 3')

        # Kick off the migration
        schema_version = self.db.SCHEMA_VERSION
        self.db.SCHEMA_VERSION = 4
        self.db._setup()
        self.db.SCHEMA_VERSION = schema_version

        # Make sure entries from the receipts table are preserved
        cursor.execute('''SELECT COUNT(receipt_id) FROM receipts''')
        self.assertEqual(1, cursor.fetchone()[0])

        # Make sure transaction IDs can be set to NULL
        cursor.execute('UPDATE receipts SET first_ta_id = NULL, last_ta_id = NULL')
