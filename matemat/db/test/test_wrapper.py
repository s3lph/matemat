
import unittest

import sqlite3

from matemat.db import DatabaseWrapper


class DatabaseTest(unittest.TestCase):

    def setUp(self) -> None:
        # Create an in-memory database for testing
        self.db = DatabaseWrapper(':memory:')

    def test_create_schema(self) -> None:
        """
        Test creation of database schema in an empty database
        """
        with self.db as db:
            self.assertEqual(DatabaseWrapper.SCHEMA_VERSION, db._user_version)

    def test_in_transaction(self) -> None:
        """
        Test transaction tracking
        """
        with self.db as db:
            self.assertFalse(db.in_transaction())
            with self.db.transaction():
                self.assertTrue(db.in_transaction())
            self.assertFalse(db.in_transaction())

    def test_transaction_nesting(self) -> None:
        """
        Inner transactions should not do anything
        """
        with self.db as db:
            self.assertFalse(db.in_transaction())
            t1 = self.db.transaction()
            with t1:
                self.assertTrue(db.in_transaction())
                self.assertFalse(t1._is_dummy)
                t2 = self.db.transaction()
                with t2:
                    self.assertTrue(db.in_transaction())
                    self.assertTrue(t2._is_dummy)
                self.assertTrue(db.in_transaction())
            self.assertFalse(db.in_transaction())

    def test_transaction_commit(self) -> None:
        """
        If no error occurs, actions in a transaction should be committed.
        """
        with self.db as db:
            with db.transaction() as c:
                c.execute('''
                INSERT INTO users VALUES (1, 'testuser', NULL, 'supersecurepassword', NULL, 1, 1, 0, 42, 0, 0)
                ''')
            c = db._sqlite_db.cursor()
            c.execute("SELECT * FROM users")
            user = c.fetchone()
            self.assertEqual((1, 'testuser', None, 'supersecurepassword', None, 1, 1, 0, 42, 0, 0), user)

    def test_transaction_rollback(self) -> None:
        """
        If an error occurs in a transaction, actions should be rolled back.
        """
        with self.db as db:
            try:
                with db.transaction() as c:
                    c.execute('''
                      INSERT INTO users VALUES (1, 'testuser', NULL, 'supersecurepassword', NULL, 1, 1, 0, 42, 0, 0)
                    ''')
                    raise ValueError('This should trigger a rollback')
            except ValueError as e:
                if str(e) != 'This should trigger a rollback':
                    raise e
            c = db._sqlite_db.cursor()
            c.execute("SELECT * FROM users")
            self.assertIsNone(c.fetchone())

    def test_connect_twice(self):
        """
        If a connection is already established, a RuntimeError should be raised when attempting to connect a
        second time.
        """
        self.db.connect()
        with self.assertRaises(RuntimeError):
            self.db.connect()
        self.db.close()

    def test_close_not_opened(self):
        """
        Attempting to close an unopened connection should raise a RuntimeError.
        """
        with self.assertRaises(RuntimeError):
            self.db.close()

    def test_close_in_transaction(self):
        """
        Attempting to close a connection inside a transaction should raise a RuntimeError.
        """
        with self.db as db:
            with db.transaction():
                with self.assertRaises(RuntimeError):
                    self.db.close()

    def test_use_before_open(self):
        with self.assertRaises(RuntimeError):
            with self.db.transaction():
                pass
        with self.assertRaises(RuntimeError):
            self.db.close()
        with self.assertRaises(RuntimeError):
            _ = self.db._user_version
        with self.assertRaises(RuntimeError):
            self.db._user_version = 42

    def test_setup_prevent_downgrade(self):
        self.db._sqlite_db = sqlite3.connect(':memory:')
        self.db._user_version = 1337
        with self.assertRaises(RuntimeError):
            self.db._setup()
