from typing import Dict, List

SCHEMAS: Dict[int, List[str]] = dict()

SCHEMAS[1] = [
    '''
    CREATE TABLE users (
      user_id INTEGER PRIMARY KEY,
      username TEXT UNIQUE NOT NULL,
      email TEXT DEFAULT NULL,
      password TEXT NOT NULL,
      touchkey TEXT DEFAULT NULL,
      is_admin INTEGER(1) NOT NULL DEFAULT 0,
      is_member INTEGER(1) NOT NULL DEFAULT 1,
      balance INTEGER(8) NOT NULL DEFAULT 0,
      lastchange INTEGER(8) NOT NULL DEFAULT 0
    );
    ''',
    '''
    CREATE TABLE products (
      product_id INTEGER PRIMARY KEY,
      name TEXT UNIQUE NOT NULL,
      stock INTEGER(8) NOT NULL DEFAULT 0,
      price_member INTEGER(8) NOT NULL,
      price_non_member INTEGER(8) NOT NULL
    );
    ''',
    '''
    CREATE TABLE consumption (
      user_id INTEGER NOT NULL,
      product_id INTEGER NOT NULL,
      count INTEGER(8) NOT NULL DEFAULT 0,
      PRIMARY KEY (user_id, product_id),
      FOREIGN KEY (user_id) REFERENCES users(user_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
      FOREIGN KEY (product_id) REFERENCES products(product_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''']

SCHEMAS[2] = [
    '''
    CREATE TABLE users (
      user_id INTEGER PRIMARY KEY,
      username TEXT UNIQUE NOT NULL,
      email TEXT DEFAULT NULL,
      password TEXT NOT NULL,
      touchkey TEXT DEFAULT NULL,
      is_admin INTEGER(1) NOT NULL DEFAULT 0,
      is_member INTEGER(1) NOT NULL DEFAULT 1,
      balance INTEGER(8) NOT NULL DEFAULT 0,
      lastchange INTEGER(8) NOT NULL DEFAULT 0
    );
    ''',
    '''
    CREATE TABLE products (
      product_id INTEGER PRIMARY KEY,
      name TEXT UNIQUE NOT NULL,
      stock INTEGER(8) NOT NULL DEFAULT 0,
      price_member INTEGER(8) NOT NULL,
      price_non_member INTEGER(8) NOT NULL
    );
    ''',
    '''
    CREATE TABLE transactions (  -- "superclass" of the following 3 tables
      ta_id INTEGER PRIMARY KEY,
      user_id INTEGER NOT NULL,
      value INTEGER(8) NOT NULL,
      old_balance INTEGER(8) NOT NULL,
      date INTEGER(8) DEFAULT (STRFTIME('%s', 'now')),
      FOREIGN KEY (user_id) REFERENCES users(user_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE consumptions (  -- transactions involving buying a product
      ta_id INTEGER PRIMARY KEY,
      product_id INTEGER DEFAULT NULL,
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
      FOREIGN KEY (product_id) REFERENCES products(product_id)
        ON DELETE SET NULL ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE deposits (  -- transactions involving depositing cash
      ta_id INTEGER PRIMARY KEY,
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE modifications (  -- transactions involving balance modification by an admin
      ta_id INTEGER NOT NULL,
      agent_id INTEGER NOT NULL,
      reason TEXT DEFAULT NULL,
      PRIMARY KEY (ta_id),
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
      FOREIGN KEY (agent_id) REFERENCES users(user_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''']

SCHEMAS[3] = [
    '''
    CREATE TABLE users (
      user_id INTEGER PRIMARY KEY,
      username TEXT UNIQUE NOT NULL,
      email TEXT DEFAULT NULL,
      password TEXT NOT NULL,
      touchkey TEXT DEFAULT NULL,
      is_admin INTEGER(1) NOT NULL DEFAULT 0,
      is_member INTEGER(1) NOT NULL DEFAULT 1,
      balance INTEGER(8) NOT NULL DEFAULT 0,
      lastchange INTEGER(8) NOT NULL DEFAULT 0,
      receipt_pref INTEGER(1) NOT NULL DEFAULT 0,
      created INTEGER(8) NOT NULL DEFAULT 0
    );
    ''',
    '''
    CREATE TABLE products (
      product_id INTEGER PRIMARY KEY,
      name TEXT UNIQUE NOT NULL,
      stock INTEGER(8) NOT NULL DEFAULT 0,
      price_member INTEGER(8) NOT NULL,
      price_non_member INTEGER(8) NOT NULL
    );
    ''',
    '''
    CREATE TABLE transactions (  -- "superclass" of the following 3 tables
      ta_id INTEGER PRIMARY KEY,
      user_id INTEGER DEFAULT NULL,
      value INTEGER(8) NOT NULL,
      old_balance INTEGER(8) NOT NULL,
      date INTEGER(8) DEFAULT (STRFTIME('%s', 'now')),
      FOREIGN KEY (user_id) REFERENCES users(user_id)
        ON DELETE SET NULL ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE consumptions (  -- transactions involving buying a product
      ta_id INTEGER PRIMARY KEY,
      product TEXT NOT NULL,
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE deposits (  -- transactions involving depositing cash
      ta_id INTEGER PRIMARY KEY,
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE modifications (  -- transactions involving balance modification by an admin
      ta_id INTEGER NOT NULL,
      agent TEXT NOT NULL,
      reason TEXT DEFAULT NULL,
      PRIMARY KEY (ta_id),
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE receipts (  -- receipts sent to the users
      receipt_id INTEGER PRIMARY KEY,
      user_id INTEGER NOT NULL,
      first_ta_id INTEGER NOT NULL,
      last_ta_id INTEGER NOT NULL,
      date INTEGER(8) DEFAULT (STRFTIME('%s', 'now')),
      FOREIGN KEY (user_id) REFERENCES users(user_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
      FOREIGN KEY (first_ta_id) REFERENCES transactions(ta_id)
        ON DELETE SET NULL ON UPDATE CASCADE,
      FOREIGN KEY (last_ta_id) REFERENCES transactions(ta_id)
        ON DELETE SET NULL ON UPDATE CASCADE
    );
    ''']

SCHEMAS[4] = [
    '''
    CREATE TABLE users (
      user_id INTEGER PRIMARY KEY,
      username TEXT UNIQUE NOT NULL,
      email TEXT DEFAULT NULL,
      password TEXT NOT NULL,
      touchkey TEXT DEFAULT NULL,
      is_admin INTEGER(1) NOT NULL DEFAULT 0,
      is_member INTEGER(1) NOT NULL DEFAULT 1,
      balance INTEGER(8) NOT NULL DEFAULT 0,
      lastchange INTEGER(8) NOT NULL DEFAULT 0,
      receipt_pref INTEGER(1) NOT NULL DEFAULT 0,
      created INTEGER(8) NOT NULL DEFAULT 0
    );
    ''',
    '''
    CREATE TABLE products (
      product_id INTEGER PRIMARY KEY,
      name TEXT UNIQUE NOT NULL,
      stock INTEGER(8) NOT NULL DEFAULT 0,
      price_member INTEGER(8) NOT NULL,
      price_non_member INTEGER(8) NOT NULL
    );
    ''',
    '''
    CREATE TABLE transactions (  -- "superclass" of the following 3 tables
      ta_id INTEGER PRIMARY KEY,
      user_id INTEGER DEFAULT NULL,
      value INTEGER(8) NOT NULL,
      old_balance INTEGER(8) NOT NULL,
      date INTEGER(8) DEFAULT (STRFTIME('%s', 'now')),
      FOREIGN KEY (user_id) REFERENCES users(user_id)
        ON DELETE SET NULL ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE consumptions (  -- transactions involving buying a product
      ta_id INTEGER PRIMARY KEY,
      product TEXT NOT NULL,
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE deposits (  -- transactions involving depositing cash
      ta_id INTEGER PRIMARY KEY,
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE modifications (  -- transactions involving balance modification by an admin
      ta_id INTEGER NOT NULL,
      agent TEXT NOT NULL,
      reason TEXT DEFAULT NULL,
      PRIMARY KEY (ta_id),
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE receipts (  -- receipts sent to the users
      receipt_id INTEGER PRIMARY KEY,
      user_id INTEGER NOT NULL,
      first_ta_id INTEGER DEFAULT NULL,
      last_ta_id INTEGER DEFAULT NULL,
      date INTEGER(8) DEFAULT (STRFTIME('%s', 'now')),
      FOREIGN KEY (user_id) REFERENCES users(user_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
      FOREIGN KEY (first_ta_id) REFERENCES transactions(ta_id)
        ON DELETE SET NULL ON UPDATE CASCADE,
      FOREIGN KEY (last_ta_id) REFERENCES transactions(ta_id)
        ON DELETE SET NULL ON UPDATE CASCADE
    );
    ''']

SCHEMAS[5] = [
    '''
    CREATE TABLE users (
      user_id INTEGER PRIMARY KEY,
      username TEXT UNIQUE NOT NULL,
      email TEXT DEFAULT NULL,
      password TEXT NOT NULL,
      touchkey TEXT DEFAULT NULL,
      is_admin INTEGER(1) NOT NULL DEFAULT 0,
      is_member INTEGER(1) NOT NULL DEFAULT 1,
      balance INTEGER(8) NOT NULL DEFAULT 0,
      lastchange INTEGER(8) NOT NULL DEFAULT 0,
      receipt_pref INTEGER(1) NOT NULL DEFAULT 0,
      created INTEGER(8) NOT NULL DEFAULT 0
    );
    ''',
    '''
    CREATE TABLE products (
      product_id INTEGER PRIMARY KEY,
      name TEXT UNIQUE NOT NULL,
      stock INTEGER(8) DEFAULT 0,
      stockable INTEGER(1) DEFAULT 1,
      price_member INTEGER(8) NOT NULL,
      price_non_member INTEGER(8) NOT NULL
    );
    ''',
    '''
    CREATE TABLE transactions (  -- "superclass" of the following 3 tables
      ta_id INTEGER PRIMARY KEY,
      user_id INTEGER DEFAULT NULL,
      value INTEGER(8) NOT NULL,
      old_balance INTEGER(8) NOT NULL,
      date INTEGER(8) DEFAULT (STRFTIME('%s', 'now')),
      FOREIGN KEY (user_id) REFERENCES users(user_id)
        ON DELETE SET NULL ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE consumptions (  -- transactions involving buying a product
      ta_id INTEGER PRIMARY KEY,
      product TEXT NOT NULL,
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE deposits (  -- transactions involving depositing cash
      ta_id INTEGER PRIMARY KEY,
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE modifications (  -- transactions involving balance modification by an admin
      ta_id INTEGER NOT NULL,
      agent TEXT NOT NULL,
      reason TEXT DEFAULT NULL,
      PRIMARY KEY (ta_id),
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE receipts (  -- receipts sent to the users
      receipt_id INTEGER PRIMARY KEY,
      user_id INTEGER NOT NULL,
      first_ta_id INTEGER DEFAULT NULL,
      last_ta_id INTEGER DEFAULT NULL,
      date INTEGER(8) DEFAULT (STRFTIME('%s', 'now')),
      FOREIGN KEY (user_id) REFERENCES users(user_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
      FOREIGN KEY (first_ta_id) REFERENCES transactions(ta_id)
        ON DELETE SET NULL ON UPDATE CASCADE,
      FOREIGN KEY (last_ta_id) REFERENCES transactions(ta_id)
        ON DELETE SET NULL ON UPDATE CASCADE
    );
    ''']


SCHEMAS[6] = [
    '''
    CREATE TABLE users (
      user_id INTEGER PRIMARY KEY,
      username TEXT UNIQUE NOT NULL,
      email TEXT DEFAULT NULL,
      password TEXT NOT NULL,
      touchkey TEXT DEFAULT NULL,
      is_admin INTEGER(1) NOT NULL DEFAULT 0,
      is_member INTEGER(1) NOT NULL DEFAULT 1,
      balance INTEGER(8) NOT NULL DEFAULT 0,
      lastchange INTEGER(8) NOT NULL DEFAULT 0,
      receipt_pref INTEGER(1) NOT NULL DEFAULT 0,
      created INTEGER(8) NOT NULL DEFAULT 0
    );
    ''',
    '''
    CREATE TABLE products (
      product_id INTEGER PRIMARY KEY,
      name TEXT UNIQUE NOT NULL,
      stock INTEGER(8) DEFAULT 0,
      stockable INTEGER(1) DEFAULT 1,
      price_member INTEGER(8) NOT NULL,
      price_non_member INTEGER(8) NOT NULL,
      custom_price INTEGER(1) DEFAULT 0
    );
    ''',
    '''
    CREATE TABLE transactions (  -- "superclass" of the following 3 tables
      ta_id INTEGER PRIMARY KEY,
      user_id INTEGER DEFAULT NULL,
      value INTEGER(8) NOT NULL,
      old_balance INTEGER(8) NOT NULL,
      date INTEGER(8) DEFAULT (STRFTIME('%s', 'now')),
      FOREIGN KEY (user_id) REFERENCES users(user_id)
        ON DELETE SET NULL ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE consumptions (  -- transactions involving buying a product
      ta_id INTEGER PRIMARY KEY,
      product TEXT NOT NULL,
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE deposits (  -- transactions involving depositing cash
      ta_id INTEGER PRIMARY KEY,
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE modifications (  -- transactions involving balance modification by an admin
      ta_id INTEGER NOT NULL,
      agent TEXT NOT NULL,
      reason TEXT DEFAULT NULL,
      PRIMARY KEY (ta_id),
      FOREIGN KEY (ta_id) REFERENCES transactions(ta_id)
        ON DELETE CASCADE ON UPDATE CASCADE
    );
    ''',
    '''
    CREATE TABLE receipts (  -- receipts sent to the users
      receipt_id INTEGER PRIMARY KEY,
      user_id INTEGER NOT NULL,
      first_ta_id INTEGER DEFAULT NULL,
      last_ta_id INTEGER DEFAULT NULL,
      date INTEGER(8) DEFAULT (STRFTIME('%s', 'now')),
      FOREIGN KEY (user_id) REFERENCES users(user_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
      FOREIGN KEY (first_ta_id) REFERENCES transactions(ta_id)
        ON DELETE SET NULL ON UPDATE CASCADE,
      FOREIGN KEY (last_ta_id) REFERENCES transactions(ta_id)
        ON DELETE SET NULL ON UPDATE CASCADE
    );
    ''']
