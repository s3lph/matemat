
from typing import Optional

from matemat.db.primitives.ReceiptPreference import ReceiptPreference


class User:
    """
    Representation of a user registered with the Matemat, with a name, e-mail address (optional), whether the user is a
    member of the organization the Matemat instance is used in, whether the user is an administrator, and the user's
    account balance.

    :param _id: The user ID in the database.
    :param name: The user's name.
    :param balance: The balance of the user's account.
    :param email: The user's e-mail address (optional).
    :param is_admin: Whether the user is an administrator.
    :param is_member: Whether the user is a member.
    :param receipt_pref: The user's preference on how often to receive transaction receipts.
    """

    def __init__(self,
                 _id: int,
                 name: str,
                 balance: int,
                 email: Optional[str] = None,
                 is_admin: bool = False,
                 is_member: bool = False,
                 receipt_pref: ReceiptPreference = ReceiptPreference.NONE) -> None:
        self.id: int = _id
        self.name: str = name
        self.balance: int = balance
        self.email: Optional[str] = email
        self.is_admin: bool = is_admin
        self.is_member: bool = is_member
        self.receipt_pref: ReceiptPreference = receipt_pref

    def __eq__(self, other) -> bool:
        if not isinstance(other, User):
            return False
        return self.id == other.id and \
            self.name == other.name and \
            self.balance == other.balance and \
            self.email == other.email and \
            self.is_admin == other.is_admin and \
            self.is_member == other.is_member and \
            self.receipt_pref == other.receipt_pref

    def __hash__(self) -> int:
        return hash((self.id, self.name, self.balance, self.email, self.is_admin, self.is_member, self.receipt_pref))
