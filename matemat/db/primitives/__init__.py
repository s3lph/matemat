"""
This package provides the 'primitive types' the Matemat software deals with - namely users and products.
"""

from .User import User
from .Product import Product
from .ReceiptPreference import ReceiptPreference
from .Transaction import Transaction, Consumption, Deposit, Modification
from .Receipt import Receipt
