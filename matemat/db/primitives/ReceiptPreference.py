from typing import Callable

from enum import Enum

from datetime import datetime, timedelta
from matemat.util.monthdelta import add_months


class ReceiptPreference(Enum):
    """
    A user's preference for the frequency of receiving receipts.
    """

    def __new__(cls, *args, **kwargs) -> 'ReceiptPreference':
        e = object.__new__(cls)
        # The enum's internal value
        e._value_: int = args[0]
        # The function calculating the date after which a new receipt is due.
        e._datefunc: Callable[[datetime], datetime] = args[1]
        # The human-readable description
        e._human_readable: str = args[2]
        return e

    @property
    def human_readable(self) -> str:
        """
        A human-readable description of the receipt preference, to be displayed in the UI.
        """
        return self._human_readable

    def next_receipt_due(self, d: datetime) -> datetime:
        return self._datefunc(d)

    """
    No receipts should be generated.
    """
    NONE = 0, (lambda d: None), 'No receipts'

    """
    A receipt should be generated once a week.
    """
    WEEKLY = 1, (lambda d: d + timedelta(weeks=1)), 'Weekly'

    """
    A receipt should be generated once a month.
    """
    MONTHLY = 2, (lambda d: add_months(d, 1)), 'Monthly'

    """
    A receipt should be generated once every three month.
    """
    QUARTERLY = 3, (lambda d: add_months(d, 3)), 'Quarterly'

    """
    A receipt should be generated once every six month.
    """
    BIANNUALLY = 4, (lambda d: add_months(d, 6)), 'Biannually'

    """
    A receipt should be generated once a year.
    """
    YEARLY = 5, (lambda d: add_months(d, 12)), 'Annually'
