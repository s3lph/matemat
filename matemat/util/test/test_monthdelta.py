
import unittest

from datetime import datetime

from matemat.util.monthdelta import add_months


class TestMonthDelta(unittest.TestCase):

    def test_monthdelta_zero(self):
        date = datetime(2018, 9, 8, 13, 37, 42)
        offset_date = date
        self.assertEqual(offset_date, add_months(date, 0))

    def test_monthdelta_one(self):
        date = datetime(2018, 9, 8, 13, 37, 42)
        offset_date = date.replace(month=10)
        self.assertEqual(offset_date, add_months(date, 1))

    def test_monthdelta_two(self):
        date = datetime(2018, 9, 8, 13, 37, 42)
        offset_date = date.replace(month=11)
        self.assertEqual(offset_date, add_months(date, 2))

    def test_monthdelta_yearwrap(self):
        date = datetime(2018, 9, 8, 13, 37, 42)
        offset_date = date.replace(year=2019, month=1)
        self.assertEqual(offset_date, add_months(date, 4))

    def test_monthdelta_yearwrap_five(self):
        date = datetime(2018, 9, 8, 13, 37, 42)
        offset_date = date.replace(year=2023, month=3)
        self.assertEqual(offset_date, add_months(date, 54))

    def test_monthdelta_rounddown_31_30(self):
        date = datetime(2018, 3, 31, 13, 37, 42)
        offset_date = date.replace(month=4, day=30)
        self.assertEqual(offset_date, add_months(date, 1))

    def test_monthdelta_rounddown_feb(self):
        date = datetime(2018, 1, 31, 13, 37, 42)
        offset_date = date.replace(month=2, day=28)
        self.assertEqual(offset_date, add_months(date, 1))

    def test_monthdelta_rounddown_feb_leap(self):
        date = datetime(2020, 1, 31, 13, 37, 42)
        offset_date = date.replace(month=2, day=29)
        self.assertEqual(offset_date, add_months(date, 1))

    def test_fail_negative(self):
        date = datetime(2020, 1, 31, 13, 37, 42)
        with self.assertRaises(ValueError):
            add_months(date, -1)

    def test_fail_type(self):
        date = datetime(2020, 1, 31, 13, 37, 42)
        with self.assertRaises(TypeError):
            add_months(date, 1.2)
        with self.assertRaises(TypeError):
            add_months(42, 1)
