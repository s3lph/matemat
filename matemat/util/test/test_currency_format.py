
import unittest

from matemat.util.currency_format import format_chf, parse_chf


class TestCurrencyFormat(unittest.TestCase):

    def test_format_zero(self):
        self.assertEqual('CHF 0.00', format_chf(0))
        self.assertEqual('0.00', format_chf(0, False))
        self.assertEqual('CHF +0.00', format_chf(0, plus_sign=True))
        self.assertEqual('+0.00', format_chf(0, False, plus_sign=True))

    def test_format_positive_full(self):
        self.assertEqual('CHF 42.00', format_chf(4200))
        self.assertEqual('42.00', format_chf(4200, False))
        self.assertEqual('CHF +42.00', format_chf(4200, plus_sign=True))
        self.assertEqual('+42.00', format_chf(4200, False, plus_sign=True))

    def test_format_negative_full(self):
        self.assertEqual('CHF -42.00', format_chf(-4200))
        self.assertEqual('-42.00', format_chf(-4200, False))
        self.assertEqual('CHF -42.00', format_chf(-4200, plus_sign=True))
        self.assertEqual('-42.00', format_chf(-4200, False, plus_sign=True))

    def test_format_positive_frac(self):
        self.assertEqual('CHF 13.37', format_chf(1337))
        self.assertEqual('13.37', format_chf(1337, False))
        self.assertEqual('CHF +13.37', format_chf(1337, plus_sign=True))
        self.assertEqual('+13.37', format_chf(1337, False, plus_sign=True))

    def test_format_negative_frac(self):
        self.assertEqual('CHF -13.37', format_chf(-1337))
        self.assertEqual('-13.37', format_chf(-1337, False))
        self.assertEqual('CHF -13.37', format_chf(-1337, plus_sign=True))
        self.assertEqual('-13.37', format_chf(-1337, False, plus_sign=True))

    def test_format_pad_left_positive(self):
        self.assertEqual('CHF 0.01', format_chf(1))
        self.assertEqual('0.01', format_chf(1, False))
        self.assertEqual('CHF +0.01', format_chf(1, plus_sign=True))
        self.assertEqual('+0.01', format_chf(1, False, plus_sign=True))

    def test_format_pad_left_negative(self):
        self.assertEqual('CHF -0.01', format_chf(-1))
        self.assertEqual('-0.01', format_chf(-1, False))
        self.assertEqual('CHF -0.01', format_chf(-1, plus_sign=True))
        self.assertEqual('-0.01', format_chf(-1, False, plus_sign=True))

    def test_format_pad_right_positive(self):
        self.assertEqual('CHF 4.20', format_chf(420))
        self.assertEqual('4.20', format_chf(420, False))
        self.assertEqual('CHF +4.20', format_chf(420, plus_sign=True))
        self.assertEqual('+4.20', format_chf(420, False, plus_sign=True))

    def test_format_pad_right_negative(self):
        self.assertEqual('CHF -4.20', format_chf(-420))
        self.assertEqual('-4.20', format_chf(-420, False))
        self.assertEqual('CHF -4.20', format_chf(-420, plus_sign=True))
        self.assertEqual('-4.20', format_chf(-420, False, plus_sign=True))

    def test_parse_empty(self):
        with self.assertRaises(ValueError):
            parse_chf('')
        with self.assertRaises(ValueError):
            parse_chf('CHF')
        with self.assertRaises(ValueError):
            parse_chf('CHF ')

    def test_parse_zero(self):
        self.assertEqual(0, parse_chf('CHF0'))
        self.assertEqual(0, parse_chf('CHF-0'))
        self.assertEqual(0, parse_chf('CHF+0'))
        self.assertEqual(0, parse_chf('CHF 0'))
        self.assertEqual(0, parse_chf('CHF +0'))
        self.assertEqual(0, parse_chf('CHF -0'))
        self.assertEqual(0, parse_chf('CHF 0.'))
        self.assertEqual(0, parse_chf('CHF 0.0'))
        self.assertEqual(0, parse_chf('CHF 0.00'))
        self.assertEqual(0, parse_chf('CHF +0.'))
        self.assertEqual(0, parse_chf('CHF +0.0'))
        self.assertEqual(0, parse_chf('CHF +0.00'))
        self.assertEqual(0, parse_chf('CHF -0.'))
        self.assertEqual(0, parse_chf('CHF -0.0'))
        self.assertEqual(0, parse_chf('CHF -0.00'))
        self.assertEqual(0, parse_chf('0'))
        self.assertEqual(0, parse_chf('+0'))
        self.assertEqual(0, parse_chf('-0'))
        self.assertEqual(0, parse_chf('0.'))
        self.assertEqual(0, parse_chf('0.0'))
        self.assertEqual(0, parse_chf('0.00'))
        self.assertEqual(0, parse_chf('+0.'))
        self.assertEqual(0, parse_chf('+0.0'))
        self.assertEqual(0, parse_chf('+0.00'))
        self.assertEqual(0, parse_chf('-0.'))
        self.assertEqual(0, parse_chf('-0.0'))
        self.assertEqual(0, parse_chf('-0.00'))

    def test_parse_positive_full(self):
        self.assertEqual(4200, parse_chf('CHF 42.00'))
        self.assertEqual(4200, parse_chf('42.00'))
        self.assertEqual(4200, parse_chf('CHF 42'))
        self.assertEqual(4200, parse_chf('42'))
        self.assertEqual(4200, parse_chf('CHF 42.'))
        self.assertEqual(4200, parse_chf('42.'))
        self.assertEqual(4200, parse_chf('CHF 42.0'))
        self.assertEqual(4200, parse_chf('42.0'))

    def test_parse_positive_full_with_sign(self):
        self.assertEqual(4200, parse_chf('CHF +42.00'))
        self.assertEqual(4200, parse_chf('+42.00'))
        self.assertEqual(4200, parse_chf('CHF +42'))
        self.assertEqual(4200, parse_chf('+42'))
        self.assertEqual(4200, parse_chf('CHF +42.'))
        self.assertEqual(4200, parse_chf('+42.'))
        self.assertEqual(4200, parse_chf('CHF +42.0'))
        self.assertEqual(4200, parse_chf('+42.0'))

    def test_parse_negative_full(self):
        self.assertEqual(-4200, parse_chf('CHF -42.00'))
        self.assertEqual(-4200, parse_chf('-42.00'))
        self.assertEqual(-4200, parse_chf('CHF -42'))
        self.assertEqual(-4200, parse_chf('-42'))
        self.assertEqual(-4200, parse_chf('CHF -42.'))
        self.assertEqual(-4200, parse_chf('-42.'))
        self.assertEqual(-4200, parse_chf('CHF -42.0'))
        self.assertEqual(-4200, parse_chf('-42.0'))

    def test_parse_positive_frac(self):
        self.assertEqual(1337, parse_chf('CHF 13.37'))
        self.assertEqual(1337, parse_chf('13.37'))

    def test_parse_positive_frac_with_sign(self):
        self.assertEqual(1337, parse_chf('CHF +13.37'))
        self.assertEqual(1337, parse_chf('+13.37'))

    def test_parse_negative_frac(self):
        self.assertEqual(-1337, parse_chf('CHF -13.37'))
        self.assertEqual(-1337, parse_chf('-13.37'))

    def test_parse_pad_left_positive(self):
        self.assertEqual(1, parse_chf('CHF 0.01'))
        self.assertEqual(1, parse_chf('0.01'))

    def test_parse_pad_left_positive_with_sign(self):
        self.assertEqual(1, parse_chf('CHF +0.01'))
        self.assertEqual(1, parse_chf('+0.01'))

    def test_parse_pad_left_negative(self):
        self.assertEqual(-1, parse_chf('CHF -0.01'))
        self.assertEqual(-1, parse_chf('-0.01'))

    def test_parse_pad_right_positive(self):
        self.assertEqual(420, parse_chf('CHF 4.20'))
        self.assertEqual(420, parse_chf('4.20'))
        self.assertEqual(420, parse_chf('CHF 4.2'))
        self.assertEqual(420, parse_chf('4.2'))

    def test_parse_pad_right_positive_with_sign(self):
        self.assertEqual(420, parse_chf('CHF +4.20'))
        self.assertEqual(420, parse_chf('+4.20'))
        self.assertEqual(420, parse_chf('CHF +4.2'))
        self.assertEqual(420, parse_chf('+4.2'))

    def test_parse_pad_right_negative(self):
        self.assertEqual(-420, parse_chf('CHF -4.20'))
        self.assertEqual(-420, parse_chf('-4.20'))
        self.assertEqual(-420, parse_chf('CHF -4.2'))
        self.assertEqual(-420, parse_chf('-4.2'))

    def test_parse_too_many_decimals(self):
        with self.assertRaises(ValueError):
            parse_chf('123.456')
        with self.assertRaises(ValueError):
            parse_chf('-123.456')
        with self.assertRaises(ValueError):
            parse_chf('CHF 0.456')
        with self.assertRaises(ValueError):
            parse_chf('CHF 0.450')
        with self.assertRaises(ValueError):
            parse_chf('CHF +0.456')

    def test_parse_wrong_separator(self):
        with self.assertRaises(ValueError):
            parse_chf('13,37')
        with self.assertRaises(ValueError):
            parse_chf('CHF 13,37')

    def test_parse_frac_negative(self):
        with self.assertRaises(ValueError):
            parse_chf('13.-7')
        with self.assertRaises(ValueError):
            parse_chf('CHF 13.-7')
        with self.assertRaises(ValueError):
            parse_chf('+13.-7')
        with self.assertRaises(ValueError):
            parse_chf('CHF -13.-7')
