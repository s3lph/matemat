from matemat.interfacing.dispenser import Dispenser


class NullDispenser(Dispenser):

    def dispense(self, product, amount):
        return True
