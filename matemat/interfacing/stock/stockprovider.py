
from typing import Optional

from matemat.db.primitives.Product import Product


class StockProvider:

    @classmethod
    def needs_update(cls) -> bool:
        """
        If this method returns True, `set_stock` and `update_stock` MUST be implemented.
        If this method returns False, `set_stock` or `update_stock` will never be called.
        :return: True if state needs to be updated, false otherwise.
        """
        return False

    def get_stock(self, product: Product) -> Optional[int]:
        """
        Returns the number of items in stock, if supported.
        :param product: The product to get the stock for.
        :return: Number of items in stock, or None if not supported.
        """
        pass

    def set_stock(self, product: Product, stock: int):
        """
        Sets the number of items in stock, if supported.
        :param product: The product to set the stock for.
        :param stock: The amount to set the stock to.
        """
        pass

    def update_stock(self, product: Product, stock: int) -> None:
        """
        Increases or decreases the number of items in stock, if supported.
        :param product: The product to update the stock for.
        :param stock: The amount to add to or subtract from the stock.
        """
        pass
