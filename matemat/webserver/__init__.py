"""
The Matemat Webserver.

This package provides the webserver for the Matemat software.  It uses Python's http.server and extends it with an event
API that can be used by 'pagelets' - single pages of a web service.  If a request cannot be handled by a pagelet, the
server will attempt to serve the request with a static resource in a previously configured webroot directory.
"""

from .logger import Logger
