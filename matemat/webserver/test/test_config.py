
from typing import List

from unittest import TestCase
from unittest.mock import patch

from io import StringIO
import logging
import sys

from matemat.webserver.config import parse_config_file, get_config

_EMPTY_CONFIG = ''

_FULL_CONFIG = '''
[Matemat]

Address=fe80::0123:45ff:fe67:89ab
Port = 8080

StaticPath   =/var/test/static
TemplatePath=   /var/test/templates

LogLevel = CRITICAL
LogTarget = /tmp/log/matemat_test.log

[Pagelets]
Name=Matemat
  (Unit Test)
UploadDir= /var/test/static/upload
DatabaseFile=/var/test/db/test.db

SmtpSendReceipts=1
SmtpEnforceTLS=0
SmtpFrom=matemat@example.com
SmtpSubj=Matemat Receipt
SmtpHost=smtp.example.com
SmtpPort=587
SmtpUser=matemat@example.com
SmtpPass=SuperSecurePassword

[HttpHeaders]
Content-Security-Policy = default-src: 'self';
X-I-Am-A-Header = andthisismyvalue
'''

_PARTIAL_CONFIG = '''
[Matemat]
Port=443

[Pagelets]
Name=Matemat (Unit Test 2)

SmtpSendReceipts=1

[HttpHeaders]
X-I-Am-A-Header = andthisismyothervalue
'''

_LOG_NONE_CONFIG = '''
[Matemat]
LogTarget=none
'''

_LOG_STDOUT_CONFIG = '''
[Matemat]
LogTarget=stdout
'''

_LOG_STDERR_CONFIG = '''
[Matemat]
LogTarget=stderr
'''

_LOG_GARBAGE_PORT = '''
[Matemat]
Port=iwanttobeavalidporttoo'''

_LOG_GARBAGE_LOGLEVEL = '''
[Matemat]
LogLevel=thisisnotaloglevel'''


class IterOpenMock:
    """
    Enable mocking of subsequent open() class for different files.  Usage:

    with mock.patch('builtins.open', IterOpenMock(['content 1', 'content 2'])):
        ...
            with open('foo') as f:
                # Reading from f will yield 'content 1'
            with open('foo') as f:
                # Reading from f will yield 'content 2'
    """

    def __init__(self, files: List[str]):
        self.files = files

    def __enter__(self):
        return StringIO(self.files[0])

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.files = self.files[1:]


class TestConfig(TestCase):

    def test_parse_config_empty_default_values(self):
        """
        Test that default values are set when reading an empty config file.
        """
        # Mock the open() function to return an empty config file example
        with patch('builtins.open', return_value=StringIO(_EMPTY_CONFIG)):
            # The filename is only a placeholder, file content is determined by mocking open
            parse_config_file('test')
        config = get_config()
        # Make sure all mandatory values are present
        self.assertIn('listen', config)
        self.assertIn('port', config)
        self.assertIn('staticroot', config)
        self.assertIn('templateroot', config)
        self.assertIn('log_level', config)
        self.assertIn('log_handler', config)
        self.assertIn('pagelet_variables', config)
        # Make sure all mandatory values are set to their default
        self.assertEqual('::', config['listen'])
        self.assertEqual(80, config['port'])
        self.assertEqual('/var/matemat/static', config['staticroot'])
        self.assertEqual('/var/matemat/templates', config['templateroot'])
        self.assertEqual(logging.INFO, config['log_level'])
        self.assertIsInstance(config['log_handler'], logging.StreamHandler)
        self.assertEqual(sys.stderr, config['log_handler'].stream)
        self.assertIsInstance(config['pagelet_variables'], dict)
        self.assertEqual(0, len(config['pagelet_variables']))
        self.assertIsInstance(config['headers'], dict)
        self.assertEqual(0, len(config['headers']))

    def test_parse_config_full(self):
        """
        Test that all default values are overridden by the values provided in the config file.
        """
        # Mock the open() function to return a full config file example
        with patch('builtins.open', return_value=StringIO(_FULL_CONFIG)):
            # The filename is only a placeholder, file content is determined by mocking open
            parse_config_file('test')
        config = get_config()
        # Make sure all mandatory values are present
        self.assertIn('listen', config)
        self.assertIn('port', config)
        self.assertIn('staticroot', config)
        self.assertIn('templateroot', config)
        self.assertIn('log_level', config)
        self.assertIn('log_handler', config)
        self.assertIn('pagelet_variables', config)
        self.assertIn('Name', config['pagelet_variables'])
        self.assertIn('UploadDir', config['pagelet_variables'])
        self.assertIn('DatabaseFile', config['pagelet_variables'])
        # Make sure all values are set as described in the config file
        self.assertEqual('fe80::0123:45ff:fe67:89ab', config['listen'])
        self.assertEqual(8080, config['port'])
        self.assertEqual('/var/test/static', config['staticroot'])
        self.assertEqual('/var/test/templates', config['templateroot'])
        self.assertEqual(logging.CRITICAL, config['log_level'])
        self.assertIsInstance(config['log_handler'], logging.FileHandler)
        self.assertEqual('/tmp/log/matemat_test.log', config['log_handler'].baseFilename)
        self.assertEqual('Matemat\n(Unit Test)', config['pagelet_variables']['Name'])
        self.assertEqual('/var/test/static/upload', config['pagelet_variables']['UploadDir'])
        self.assertEqual('/var/test/db/test.db', config['pagelet_variables']['DatabaseFile'])
        self.assertEqual('1', config['pagelet_variables']['SmtpSendReceipts'])
        self.assertEqual('0', config['pagelet_variables']['SmtpEnforceTLS'])
        self.assertEqual('matemat@example.com', config['pagelet_variables']['SmtpFrom'])
        self.assertEqual('Matemat Receipt', config['pagelet_variables']['SmtpSubj'])
        self.assertEqual('smtp.example.com', config['pagelet_variables']['SmtpHost'])
        self.assertEqual('587', config['pagelet_variables']['SmtpPort'])
        self.assertEqual('matemat@example.com', config['pagelet_variables']['SmtpUser'])
        self.assertEqual('SuperSecurePassword', config['pagelet_variables']['SmtpPass'])
        self.assertIsInstance(config['headers'], dict)
        self.assertEqual(2, len(config['headers']))
        self.assertEqual('default-src: \'self\';', config['headers']['Content-Security-Policy'])
        self.assertEqual('andthisismyvalue', config['headers']['X-I-Am-A-Header'])

    def test_parse_config_precedence(self):
        """
        Test that config items from files with higher precedence replace items with the same key from files with lower
        precedence.
        """
        # Mock the open() function to return a full config file on the first call, and a partial config file on the
        # second call
        with patch('builtins.open', return_value=IterOpenMock([_FULL_CONFIG, _PARTIAL_CONFIG])):
            # These filenames are only placeholders, file content is determined by mocking open
            parse_config_file(['full', 'partial'])
        config = get_config()
        # Make sure all mandatory values are present
        self.assertIn('listen', config)
        self.assertIn('port', config)
        self.assertIn('staticroot', config)
        self.assertIn('templateroot', config)
        self.assertIn('pagelet_variables', config)
        self.assertIn('Name', config['pagelet_variables'])
        self.assertIn('UploadDir', config['pagelet_variables'])
        self.assertIn('DatabaseFile', config['pagelet_variables'])
        # Make sure all values are set as described in the config files, values from the partial file take precedence
        self.assertEqual('fe80::0123:45ff:fe67:89ab', config['listen'])
        self.assertEqual(443, config['port'])
        self.assertEqual('/var/test/static', config['staticroot'])
        self.assertEqual('/var/test/templates', config['templateroot'])
        self.assertEqual('Matemat (Unit Test 2)', config['pagelet_variables']['Name'])
        self.assertEqual('/var/test/static/upload', config['pagelet_variables']['UploadDir'])
        self.assertEqual('/var/test/db/test.db', config['pagelet_variables']['DatabaseFile'])
        self.assertIsInstance(config['headers'], dict)
        self.assertEqual(2, len(config['headers']))
        self.assertEqual('default-src: \'self\';', config['headers']['Content-Security-Policy'])
        self.assertEqual('andthisismyothervalue', config['headers']['X-I-Am-A-Header'])

    def test_parse_config_logging_none(self):
        """
        Test that "LogTaget=none" disables logging.
        """
        # Mock the open() function to return a config file example with disabled logging
        with patch('builtins.open', return_value=StringIO(_LOG_NONE_CONFIG)):
            # The filename is only a placeholder, file content is determined by mocking open
            parse_config_file('test')
        config = get_config()
        # Make sure the returned log handler is a null handler
        self.assertIsInstance(config['log_handler'], logging.NullHandler)

    def test_parse_config_logging_stdout(self):
        """
        Test that "LogTaget=stdout" logs to sys.stdout.
        """
        # Mock the open() function to return a config file example with stdout logging
        with patch('builtins.open', return_value=StringIO(_LOG_STDOUT_CONFIG)):
            # The filename is only a placeholder, file content is determined by mocking open
            parse_config_file('test')
        config = get_config()
        # Make sure the returned log handler is a stdout handler
        self.assertIsInstance(config['log_handler'], logging.StreamHandler)
        self.assertEqual(sys.stdout, config['log_handler'].stream)

    def test_parse_config_logging_stderr(self):
        """
        Test that "LogTaget=stderr" logs to sys.stderr.
        """
        # Mock the open() function to return a config file example with stdout logging
        with patch('builtins.open', return_value=StringIO(_LOG_STDERR_CONFIG)):
            # The filename is only a placeholder, file content is determined by mocking open
            parse_config_file('test')
        config = get_config()
        # Make sure the returned log handler is a stdout handler
        self.assertIsInstance(config['log_handler'], logging.StreamHandler)
        self.assertEqual(sys.stderr, config['log_handler'].stream)

    def test_parse_config_garbage(self):
        """
        Test that garbage config raises ValueErrors.
        """
        # Mock the open() function to return a config file example with a non-numeric port
        with patch('builtins.open', return_value=StringIO(_LOG_GARBAGE_PORT)):
            # Make sure a ValueError is raised
            with self.assertRaises(ValueError):
                # The filename is only a placeholder, file content is determined by mocking open
                parse_config_file('test')
        # Mock the open() function to return a config file example with a non-numeric log level with invalid symbol
        with patch('builtins.open', return_value=StringIO(_LOG_GARBAGE_LOGLEVEL)):
            # Make sure a ValueError is raised
            with self.assertRaises(ValueError):
                # The filename is only a placeholder, file content is determined by mocking open
                parse_config_file('test')

    def test_parse_config_not_a_filename(self):
        """
        Test type checking for the config filenames
        """
        with self.assertRaises(TypeError):
            parse_config_file(42)
        with self.assertRaises(TypeError):
            parse_config_file(['config', 42])
