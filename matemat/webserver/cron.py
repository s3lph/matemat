from typing import Callable

from datetime import timedelta
from threading import Event, Timer, Thread

from matemat.webserver import Logger

_CRON_STATIC_EVENT: Event = Event()


def shutdown(*_args, **_kwargs):
    _CRON_STATIC_EVENT.set()


class _GlobalEventTimer(Thread):
    """
    A timer similar to threading.Timer, except that waits on an externally supplied threading.Event instance,
    therefore allowing all timers waiting on the same event to be cancelled at once.
    """

    def __init__(self, interval: float, event: Event, fun, *args, **kwargs):
        """
        Create a new _GlobalEventTimer.
        :param interval: The delay after which to run the function.
        :param event: The external threading.Event to wait on.
        :param fun: The function to call.
        :param args: The positional arguments to pass to the function.
        :param kwargs: The keyword arguments to pass to the function.
        """
        Thread.__init__(self)
        self.interval = interval
        self.fun = fun
        self.args = args if args is not None else []
        self.kwargs = kwargs if kwargs is not None else {}
        self.event = event

    def run(self):
        self.event.wait(self.interval)
        if not self.event.is_set():
            self.fun(*self.args, **self.kwargs)
        # Do NOT call event.set(), as done in threading.Timer, as that would cancel all other timers


def cron(weeks: int = 0,
         days: int = 0,
         hours: int = 0,
         seconds: int = 0,
         minutes: int = 0,
         milliseconds: int = 0,
         microseconds: int = 0):
    """
    Annotate a function to act as a cron function.  The function will be called in a regular interval, defined
    by the arguments passed to the decorator, which are passed to a timedelta object.

    :param weeks: Number of weeks in the interval.
    :param days: Number of days in the interval.
    :param hours: Number of hours in the interval.
    :param seconds: Number of seconds in the interval.
    :param minutes: Number of minutes in the interval.
    :param milliseconds: Number of milliseconds in the interval.
    :param microseconds: Number of microseconds in the interval.
    """

    def cron_wrapper(fun: Callable[[], None]):
        # Create the timedelta object
        delta: timedelta = timedelta(weeks=weeks,
                                     days=days,
                                     hours=hours,
                                     seconds=seconds,
                                     minutes=minutes,
                                     milliseconds=milliseconds,
                                     microseconds=microseconds)

        # This function is called once in the specified interval
        def cron():
            logger = Logger.instance()
            # Reschedule the job
            t: Timer = _GlobalEventTimer(delta.total_seconds(), _CRON_STATIC_EVENT, cron)
            t.start()
            # Actually run the task
            logger.info('Executing cron job "%s"', fun.__name__)
            try:
                fun()
                logger.info('Completed cron job "%s"', fun.__name__)
            except BaseException as e:
                logger.exception('Cron job "%s" failed:', fun.__name__, exc_info=e)

        # Set a timer to run the cron job after the specified interval
        timer: Timer = _GlobalEventTimer(delta.total_seconds(), _CRON_STATIC_EVENT, cron)
        timer.start()

    return cron_wrapper
