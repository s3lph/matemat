from typing import Any, Dict

import logging

from matemat.webserver.config import get_config


class Logger(logging.Logger):

    logger: 'Logger' = None

    def __init__(self):
        super().__init__(name='matemat')
        config: Dict[str, Any] = get_config()
        log_handler: logging.Handler = config['log_handler']
        self.setLevel(config['log_level'])
        # Set up the log handler's (obtained from config parsing) format string
        log_handler.setFormatter(logging.Formatter('%(asctime)s %(module)s [%(levelname)s]: %(message)s'))
        self.addHandler(log_handler)

    @classmethod
    def instance(cls) -> 'Logger':
        if cls.logger is None:
            cls.logger = Logger()
        return cls.logger
