from bottle import get, post, redirect, abort, request

from matemat.db import MatematDatabase
from matemat.db.primitives import User
from matemat.exceptions import AuthenticationError
from matemat.webserver import template, session
from matemat.webserver.config import get_app_config


@get('/touchkey')
@post('/touchkey')
def touchkey_page():
    """
    The touchkey login mechanism.  If called via GET, render the UI template; if called via POST, attempt to log in with
    the provided credentials (username and touchkey).
    """
    config = get_app_config()
    session_id: str = session.start()
    # If a user is already logged in, simply redirect to the main page, showing the product list
    if session.has(session_id, 'authenticated_user'):
        redirect('/')
    # If requested via HTTP GET, render the login page showing the touchkey UI
    if request.method == 'GET':
        return template.render('touchkey.html',
                               username=str(request.params.username), uid=int(str(request.params.uid)),
                               setupname=config['InstanceName'])
    # If requested via HTTP POST, read the request arguments and attempt to log in with the provided credentials
    elif request.method == 'POST':
        # Connect to the database
        with MatematDatabase(config['DatabaseFile']) as db:
            try:
                # Read the request arguments and attempt to log in with them
                user: User = db.login(str(request.params.username), touchkey=str(request.params.touchkey))
            except AuthenticationError:
                # Reload the touchkey login page on failure
                redirect(f'/touchkey?uid={str(request.params.uid)}&username={str(request.params.username)}')
        # Set the user ID session variable
        session.put(session_id, 'authenticated_user', user.id)
        # Set the authlevel session variable (0 = none, 1 = touchkey, 2 = password login)
        session.put(session_id, 'authentication_level', 1)
        # Redirect to the main page, showing the product list
        redirect('/')
    # If neither GET nor POST was used, show a 405 Method Not Allowed error page
    abort(405)
