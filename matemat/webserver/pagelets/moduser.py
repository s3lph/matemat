import os
from datetime import datetime
from io import BytesIO
from typing import Dict, Optional

import magic
from PIL import Image
from bottle import get, post, redirect, abort, request, FormsDict

from matemat.db import MatematDatabase
from matemat.db.primitives import User, ReceiptPreference
from matemat.exceptions import DatabaseConsistencyError
from matemat.util.currency_format import parse_chf
from matemat.webserver import template, session
from matemat.webserver.config import get_app_config


@get('/moduser')
@post('/moduser')
def moduser():
    """
    The user modification page available from the admin panel.
    """
    config = get_app_config()
    session_id: str = session.start()
    # If no user is logged in, redirect to the login page
    if not session.has(session_id, 'authentication_level') or not session.has(session_id, 'authenticated_user'):
        redirect('/login')
    authlevel: int = session.get(session_id, 'authentication_level')
    auth_uid: int = session.get(session_id, 'authenticated_user')
    # Show a 403 Forbidden error page if no user is logged in (0) or a user logged in via touchkey (1)
    if authlevel < 2:
        abort(403)

    # Connect to the database
    with MatematDatabase(config['DatabaseFile']) as db:
        # Fetch the authenticated user
        authuser = db.get_user(auth_uid)
        if not authuser.is_admin:
            # Show a 403 Forbidden error page if the user is not an admin
            abort(403)
        if 'userid' not in request.params:
            # Show a 400 Bad Request error page if no users to edit was specified
            # (should never happen during normal operation)
            abort(400, '"userid" argument missing')

        # Fetch the user to modify from the database
        moduser_id = int(str(request.params.userid))
        user = db.get_user(moduser_id)

        # If the request contains a "change" parameter, delegate the change handling to the function below
        if 'change' in request.params:
            handle_change(request.params, request.files, user, authuser, db)
            # If the user was deleted, redirect back to the admin page, as there is nothing to edit any more
            if str(request.params.change) == 'del':
                redirect('/admin')

        # Render the "Modify User" page
        now = str(int(datetime.utcnow().timestamp()))
        return template.render('moduser.html',
                               authuser=authuser, user=user, authlevel=authlevel, now=now,
                               receipt_preference_class=ReceiptPreference,
                               setupname=config['InstanceName'], config_smtp_enabled=config['SmtpSendReceipts'])


def handle_change(args: FormsDict, files: FormsDict, user: User, authuser: User, db: MatematDatabase) \
        -> None:
    """
    Write the changes requested by an admin to the database.

    :param args: The RequestArguments object passed to the pagelet.
    :param user: The user to edit.
    :param authuser: The user performing the modification.
    :param db: The database facade where changes are written to.
    """
    config = get_app_config()
    # Read the type of change requested by the admin, then switch over it
    change = str(args.change)

    # Admin requested deletion of the user
    if change == 'del':
        # Delete the user from the database
        db.delete_user(user)
        # Delete the user's avatar, if it exists
        try:
            abspath: str = os.path.join(os.path.abspath(config['UploadDir']), 'thumbnails/users/')
            os.remove(os.path.join(abspath, f'{user.id}.png'))
        except FileNotFoundError:
            pass

    # Admin requested update of the user's details
    elif change == 'update':
        # Only write a change if all properties of the user are present in the request arguments
        if 'username' not in args or \
                'email' not in args or \
                'password' not in args or \
                'balance' not in args or \
                'receipt_pref' not in args:
            return
        # Read the properties from the request arguments
        username = str(args.username)
        email = str(args.email)
        try:
            receipt_pref = ReceiptPreference(int(str(args.receipt_pref)))
        except ValueError:
            return
        password = str(args.password)
        balance = parse_chf(str(args.balance))
        balance_reason: Optional[str] = str(args.reason)
        if balance_reason == '':
            balance_reason = None
        is_member = 'ismember' in args
        is_admin = 'isadmin' in args
        # An empty e-mail field should be interpreted as NULL
        if len(email) == 0:
            email = None
        # Attempt to write the changes to the database
        try:
            # If a password was entered, replace the password in the database
            if len(password) > 0:
                db.change_password(user, '', password, verify_password=False)
            # Write the user detail changes
            db.change_user(user, agent=authuser, name=username, email=email, is_member=is_member, is_admin=is_admin,
                           balance=balance, balance_reason=balance_reason, receipt_pref=receipt_pref)
        except DatabaseConsistencyError:
            return
        # If a new avatar was uploaded, process it
        if 'avatar' in files:
            # Read the raw image data from the request
            avatar = files.avatar.file.read()
            # Only process the image, if its size is more than zero.  Zero size means no new image was uploaded
            if len(avatar) == 0:
                return
            # Detect the MIME type
            filemagic: magic.FileMagic = magic.detect_from_content(avatar)
            if not filemagic.mime_type.startswith('image/'):
                return
            # Create the absolute path of the upload directory
            abspath: str = os.path.join(os.path.abspath(config['UploadDir']), 'thumbnails/users/')
            os.makedirs(abspath, exist_ok=True)
            try:
                # Parse the image data
                image: Image = Image.open(BytesIO(avatar))
                # Resize the image to 150x150
                image.thumbnail((150, 150), Image.LANCZOS)
                # Write the image to the file
                image.save(os.path.join(abspath, f'{user.id}.png'), 'PNG')
            except OSError:
                return
