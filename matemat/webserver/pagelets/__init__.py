"""
This package contains the pagelet functions served by the Matemat software.

A new pagelet function must be imported here to be automatically loaded when the server is started.
"""

from .main import main_page
from .login import login_page
from .logout import logout
from .signup import signup
from .touchkey import touchkey_page
from .buy import buy
from .deposit import deposit
from .transfer import transfer
from .admin import admin
from .metrics import metrics
from .moduser import moduser
from .modproduct import modproduct
from .userbootstrap import userbootstrap
from .statistics import statistics
