from typing import List, Tuple

import logging

import smtplib as smtp
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from matemat.webserver.cron import cron
from matemat.webserver import template
from matemat.webserver.config import get_app_config
from matemat.db import MatematDatabase
from matemat.db.primitives import User, Receipt
from matemat.util.currency_format import format_chf


logger: logging.Logger = logging.Logger(__file__)


@cron(hours=6)
def receipt_smtp_cron() -> None:
    config = get_app_config()
    if config['SmtpSendReceipts'] != '1':
        # Sending receipts via mail is disabled
        return
    logger.info('Searching users due for receipts.')
    receipts: List[Receipt] = []
    # Connect to the database
    with MatematDatabase(config['DatabaseFile']) as db:
        users: List[User] = db.list_users()
        for user in users:
            if user.email is None:
                logger.debug('User "%s" has no e-mail address.', user.name)
            if db.check_receipt_due(user):
                logger.info('Generating receipt for user "%s".', user.name)
                # Generate receipts that are due
                receipt: Receipt = db.create_receipt(user, write=True)
                receipts.append(receipt)
            else:
                logger.debug('No receipt due for user "%s".', user.name)
    # Send all generated receipts via e-mail
    if len(receipts) > 0:
        _send_receipt_mails(receipts)


def _send_receipt_mails(receipts: List[Receipt]) -> None:
    config = get_app_config()
    mails: List[Tuple[str, MIMEMultipart]] = []
    for receipt in receipts:
        if receipt.user.email is None:
            continue
        # Create a new message object
        msg: MIMEMultipart = MIMEMultipart()
        msg['From'] = config['SmtpFrom']
        msg['To'] = receipt.user.email
        msg['Subject'] = config['SmtpSubj']
        # Format the receipt properties for the text representation
        fdate: str = receipt.from_date.strftime('%d.%m.%Y, %H:%M')
        tdate: str = receipt.to_date.strftime('%d.%m.%Y, %H:%M')
        username: str = receipt.user.name.rjust(40)
        if len(receipt.transactions) == 0:
            fbal: str = format_chf(receipt.user.balance).rjust(12)
        else:
            fbal = format_chf(receipt.transactions[0].old_balance).rjust(12)
        tbal: str = format_chf(receipt.user.balance).rjust(12)
        # Render the receipt
        rendered: str = template.render('receipt.txt',
                                        date=fdate, tdate=tdate, user=username, fbal=fbal, tbal=tbal,
                                        receipt_id=receipt.id, transactions=receipt.transactions,
                                        instance_name=config['InstanceName'])
        # Put the rendered receipt in the message body
        body: MIMEText = MIMEText(rendered)
        msg.attach(body)
        mails.append((receipt.user.email, msg))

    # Connect to the SMTP Server
    con: smtp.SMTP = smtp.SMTP(config['SmtpHost'], config['SmtpPort'])
    try:
        # Attempt to upgrade to a TLS connection
        try:
            con.starttls()
        except BaseException:
            # If STARTTLS failed, only continue if explicitly requested by configuration
            if config['SmtpEnforceTLS'] != '0':
                logger.error('STARTTLS not supported by SMTP server, aborting!')
                return
            else:
                logger.warning('Sending e-mails in plain text as requested by SmtpEnforceTLS=0.')
        # Send SMTP login credentials
        con.login(config['SmtpUser'], config['SmtpPass'])

        # Send the e-mails
        for to, msg in mails:
            logger.info('Sending mail to %s', to)
            con.sendmail(config['SmtpFrom'], to, msg.as_string())
    except smtp.SMTPException as e:
        logger.exception('Exception while sending receipt e-mails', exc_info=e)
    finally:
        con.close()
