from bottle import get, post, redirect, request

from matemat.db import MatematDatabase
from matemat.webserver import session
from matemat.webserver import config as c


@get('/buy')
@post('/buy')
def buy():
    """
    The purchasing mechanism.  Called by the user clicking an item on the product list.
    """
    config = c.get_app_config()
    session_id: str = session.start()
    # If no user is logged in, redirect to the main page, as a purchase must always be bound to a user
    if not session.has(session_id, 'authenticated_user'):
        redirect('/')
    # Connect to the database
    with MatematDatabase(config['DatabaseFile']) as db:
        # Fetch the authenticated user from the database
        uid: int = session.get(session_id, 'authenticated_user')
        user = db.get_user(uid)
        # Read the product from the database, identified by the product ID passed as request argument
        if 'pid' in request.params:
            pid = int(str(request.params.pid))
            product = db.get_product(pid)
            if c.get_dispenser().dispense(product, 1):
                price = product.price_member if user.is_member else product.price_non_member
                if 'price' in request.params:
                    price = int(str(request.params.price))
                # Create a consumption entry for the (user, product) combination
                db.increment_consumption(user, product, price)
                stock_provider = c.get_stock_provider()
                if stock_provider.needs_update():
                    stock_provider.update_stock(product, -1)
                # Redirect to the main page (where this request should have come from)
                redirect(f'/?lastaction=buy&lastproduct={pid}&lastprice={price}')
    redirect('/')
