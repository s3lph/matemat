import os
from io import BytesIO
from datetime import datetime
from typing import Dict

import magic
from PIL import Image
from bottle import get, post, redirect, abort, request, FormsDict

from matemat.db import MatematDatabase
from matemat.db.primitives import Product
from matemat.exceptions import DatabaseConsistencyError
from matemat.util.currency_format import parse_chf
from matemat.webserver import template, session
from matemat.webserver.config import get_app_config, get_stock_provider


@get('/modproduct')
@post('/modproduct')
def modproduct():
    """
    The product modification page available from the admin panel.
    """
    config = get_app_config()
    session_id: str = session.start()
    # If no user is logged in, redirect to the login page
    if not session.has(session_id, 'authentication_level') or not session.has(session_id, 'authenticated_user'):
        redirect('/login')
    authlevel: int = session.get(session_id, 'authentication_level')
    auth_uid: int = session.get(session_id, 'authenticated_user')
    # Show a 403 Forbidden error page if no user is logged in (0) or a user logged in via touchkey (1)
    if authlevel < 2:
        abort(403)

    # Connect to the database
    with MatematDatabase(config['DatabaseFile']) as db:
        # Fetch the authenticated user
        authuser = db.get_user(auth_uid)
        if not authuser.is_admin:
            # Show a 403 Forbidden error page if the user is not an admin
            abort(403)
        if 'productid' not in request.params:
            # Show a 400 Bad Request error page if no product to edit was specified
            # (should never happen during normal operation)
            abort(400, '"productid" argument missing')

        # Fetch the product to modify from the database
        modproduct_id = int(str(request.params.productid))
        product = db.get_product(modproduct_id)

        # If the request contains a "change" parameter, delegate the change handling to the function below
        if 'change' in request.params:
            handle_change(request.params, request.files, product, db)
            # If the product was deleted, redirect back to the admin page, as there is nothing to edit any more
            if str(request.params.change) == 'del':
                redirect('/admin')

        # Render the "Modify Product" page
        now = str(int(datetime.utcnow().timestamp()))
        return template.render('modproduct.html',
                               authuser=authuser, product=product, authlevel=authlevel,
                               setupname=config['InstanceName'], now=now)


def handle_change(args: FormsDict, files: FormsDict, product: Product, db: MatematDatabase) -> None:
    """
    Write the changes requested by an admin to the database.

    :param args: The FormsDict object passed to the pagelet.
    :param product: The product to edit.
    :param db: The database facade where changes are written to.
    """
    config = get_app_config()
    # Read the type of change requested by the admin, then switch over it
    change = str(args.change)

    # Admin requested deletion of the product
    if change == 'del':
        # Delete the product from the database
        db.delete_product(product)
        # Delete the product image, if it exists
        try:
            abspath: str = os.path.join(os.path.abspath(config['UploadDir']), 'thumbnails/products/')
            os.remove(os.path.join(abspath, f'{product.id}.png'))
        except FileNotFoundError:
            pass

    # Admin requested update of the product details
    elif change == 'update':
        # Only write a change if all properties of the product are present in the request arguments
        for key in ['name', 'pricemember', 'pricenonmember']:
            if key not in args:
                return
        # Read the properties from the request arguments
        name = str(args.name)
        price_member = parse_chf(str(args.pricemember))
        price_non_member = parse_chf(str(args.pricenonmember))
        custom_price = 'custom_price' in args
        stock = int(str(args.stock))
        stockable = 'stockable' in args
        # Attempt to write the changes to the database
        try:
            db.change_product(product,
                              name=name, price_member=price_member, price_non_member=price_non_member,
                              custom_price=custom_price, stock=stock, stockable=stockable)
            stock_provider = get_stock_provider()
            if stock_provider.needs_update() and product.stockable:
                stock_provider.set_stock(product, stock)
        except DatabaseConsistencyError:
            return
        # If a new product image was uploaded, process it
        if 'image' in files:
            # Read the raw image data from the request
            avatar = files.image.file.read()
            # Only process the image, if its size is more than zero.  Zero size means no new image was uploaded
            if len(avatar) == 0:
                return
            # Detect the MIME type
            filemagic: magic.FileMagic = magic.detect_from_content(avatar)
            if not filemagic.mime_type.startswith('image/'):
                return
            # Create the absolute path of the upload directory
            abspath: str = os.path.join(os.path.abspath(config['UploadDir']), 'thumbnails/products/')
            os.makedirs(abspath, exist_ok=True)
            try:
                # Parse the image data
                image: Image = Image.open(BytesIO(avatar))
                # Resize the image to 150x150
                image.thumbnail((150, 150), Image.LANCZOS)
                # Write the image to the file
                image.save(os.path.join(abspath, f'{product.id}.png'), 'PNG')
            except OSError:
                return
