from datetime import datetime

from bottle import route, redirect, request

from matemat.db import MatematDatabase
from matemat.webserver import template, session
from matemat.webserver.config import get_app_config, get_stock_provider


@route('/')
def main_page():
    """
    The main page, showing either the user list (if no user is logged in) or the product list (if a user is logged in).
    """
    config = get_app_config()
    session_id: str = session.start()
    now = str(int(datetime.utcnow().timestamp()))
    with MatematDatabase(config['DatabaseFile']) as db:
        # Check whether a user is logged in
        if session.has(session_id, 'authenticated_user'):
            # Fetch the user id and authentication level (touchkey vs password) from the session storage
            uid: int = session.get(session_id, 'authenticated_user')
            authlevel: int = session.get(session_id, 'authentication_level')
            # Fetch the user object from the database (for name display, price calculation and admin check)
            users = db.list_users()
            user = db.get_user(uid)
            # Fetch the list of products to display
            products = db.list_products()
            if request.params.lastproduct:
                lastproduct = db.get_product(request.params.lastproduct)
            else:
                lastproduct = None
            lastprice = int(request.params.lastprice) if request.params.lastprice else None
            # Prepare a response with a jinja2 template
            return template.render('productlist.html',
                                   authuser=user, users=users, products=products, authlevel=authlevel,
                                   lastaction=request.params.lastaction, lastprice=lastprice, lastproduct=lastproduct,
                                   stock=get_stock_provider(), setupname=config['InstanceName'], now=now)
        else:
            # If there are no admin users registered, jump to the admin creation procedure
            if not db.has_admin_users():
                redirect('/userbootstrap')
            # If no user is logged in, fetch the list of users and render the userlist template
            users = db.list_users(with_touchkey=True)
            return template.render('userlist.html',
                                   users=users, setupname=config['InstanceName'], now=now,
                                   signup=(config.get('SignupEnabled', '0') == '1'))
